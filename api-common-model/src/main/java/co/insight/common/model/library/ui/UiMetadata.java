package co.insight.common.model.library.ui;

public class UiMetadata {
  private static final long serialVersionUID = 1L;

  private long cache_version;
  private String _last_updated_at;
  private String screen_type;
  private String language;
  private String content_id;

  public long getCache_version() {
    return cache_version;
  }

  public void setCache_version(long cache_version) {
    this.cache_version = cache_version;
  }

  public String get_last_updated_at() {
    return _last_updated_at;
  }

  public void set_last_updated_at(String _last_updated_at) {
    this._last_updated_at = _last_updated_at;
  }

  public String getScreen_type() {
    return screen_type;
  }

  public void setScreen_type(String screen_type) {
    this.screen_type = screen_type;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public String getContent_id() {
    return content_id;
  }

  public void setContent_id(String content_id) {
    this.content_id = content_id;
  }
}
