package co.insight.common.model.library;

import java.io.Serializable;

public class Rating implements Serializable{
  private static final long serialVersionUID = -7997305703211075194L;
  private Double score;

  private Integer count;

  private long cache_version;

  public long getCache_version() {
    return cache_version;
  }

  public void setCache_version(long cache_version) {
    this.cache_version = cache_version;
  }

  public Double getScore() {
    return score;
  }

  public void setScore(Double score) {
    this.score = score;
  }

  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }
}
