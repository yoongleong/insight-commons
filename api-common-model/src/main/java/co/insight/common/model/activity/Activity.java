package co.insight.common.model.activity;

import co.insight.common.model.user.User;
import java.io.Serializable;

/**
 * Activity represents user's behaviour events such as start or complete meditation, accept a friend request and joined a group , etc.
 */
public class Activity implements Serializable {
  private static final long serialVersionUID = 6950057940425749952L;

  private String id;
  private User user;
  private Long length;
  private String label;
  private String subline;
  private String user_id;
  private Double progress;
  private Long started_at;
  private String target_id;
  private ActivityType type;
  private Boolean is_private;
  private String description;
  private String user_event_id;
  private ActivityTarget target;
  private String time_and_location;
  private Boolean proxied_from_ruby;
  private Long cache_version;

  public Activity() {
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Activity activity = (Activity) o;

    if (id != null ? !id.equals(activity.id) : activity.id != null) {
      return false;
    }
    if (user != null ? !user.equals(activity.user) : activity.user != null) {
      return false;
    }
    if (length != null ? !length.equals(activity.length) : activity.length != null) {
      return false;
    }
    if (label != null ? !label.equals(activity.label) : activity.label != null) {
      return false;
    }
    if (subline != null ? !subline.equals(activity.subline) : activity.subline != null) {
      return false;
    }
    if (user_id != null ? !user_id.equals(activity.user_id) : activity.user_id != null) {
      return false;
    }
    if (progress != null ? !progress.equals(activity.progress) : activity.progress != null) {
      return false;
    }
    if (started_at != null ? !started_at.equals(activity.started_at) : activity.started_at != null) {
      return false;
    }
    if (target_id != null ? !target_id.equals(activity.target_id) : activity.target_id != null) {
      return false;
    }
    if (type != activity.type) {
      return false;
    }
    if (is_private != null ? !is_private.equals(activity.is_private) : activity.is_private != null) {
      return false;
    }
    if (description != null ? !description.equals(activity.description) : activity.description != null) {
      return false;
    }
    if (user_event_id != null ? !user_event_id.equals(activity.user_event_id) : activity.user_event_id != null) {
      return false;
    }
    if (target != null ? !target.equals(activity.target) : activity.target != null) {
      return false;
    }
    if (time_and_location != null ? !time_and_location.equals(activity.time_and_location) : activity.time_and_location != null) {
      return false;
    }
    return proxied_from_ruby != null ? proxied_from_ruby.equals(activity.proxied_from_ruby) : activity.proxied_from_ruby == null;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (user != null ? user.hashCode() : 0);
    result = 31 * result + (length != null ? length.hashCode() : 0);
    result = 31 * result + (label != null ? label.hashCode() : 0);
    result = 31 * result + (subline != null ? subline.hashCode() : 0);
    result = 31 * result + (user_id != null ? user_id.hashCode() : 0);
    result = 31 * result + (progress != null ? progress.hashCode() : 0);
    result = 31 * result + (started_at != null ? started_at.hashCode() : 0);
    result = 31 * result + (target_id != null ? target_id.hashCode() : 0);
    result = 31 * result + (type != null ? type.hashCode() : 0);
    result = 31 * result + (is_private != null ? is_private.hashCode() : 0);
    result = 31 * result + (description != null ? description.hashCode() : 0);
    result = 31 * result + (user_event_id != null ? user_event_id.hashCode() : 0);
    result = 31 * result + (target != null ? target.hashCode() : 0);
    result = 31 * result + (time_and_location != null ? time_and_location.hashCode() : 0);
    result = 31 * result + (proxied_from_ruby != null ? proxied_from_ruby.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Activity{" +
        "id='" + id + '\'' +
        ", user=" + user +
        ", length=" + length +
        ", label='" + label + '\'' +
        ", subline='" + subline + '\'' +
        ", user_id='" + user_id + '\'' +
        ", progress=" + progress +
        ", started_at=" + started_at +
        ", target_id='" + target_id + '\'' +
        ", type=" + type +
        ", is_private=" + is_private +
        ", description='" + description + '\'' +
        ", user_event_id='" + user_event_id + '\'' +
        ", target=" + target +
        ", time_and_location='" + time_and_location + '\'' +
        ", proxied_from_ruby=" + proxied_from_ruby +
        ", cache_version=" + cache_version +
        '}';
  }

  public Long getCache_version() {
    return cache_version;
  }

  public void setCache_version(Long cache_version) {
    this.cache_version = cache_version;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public ActivityType getType() {
    return type;
  }

  public void setType(ActivityType type) {
    this.type = type;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public void setUserId(String user_id) {
    this.user_id = user_id;
  }

  public String getTarget_id() {
    return target_id;
  }

  public void setTarget_id(String target_id) {
    this.target_id = target_id;
  }

  public Long getStarted_at() {
    return started_at;
  }

  public void setStarted_at(Long started_at) {
    this.started_at = started_at;
  }

  public String getUser_event_id() {
    return user_event_id;
  }

  public void setUser_event_id(String user_event_id) {
    this.user_event_id = user_event_id;
  }

  public Long getLength() {
    return length;
  }

  public void setLength(Long length) {
    this.length = length;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public ActivityTarget getTarget() {
    return target;
  }

  public void setTarget(ActivityTarget target) {
    this.target = target;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getTime_and_location() {
    return time_and_location;
  }

  public void setTime_and_location(String time_and_location) {
    this.time_and_location = time_and_location;
  }

  public String getSubline() {
    return subline;
  }

  public void setSubline(String subline) {
    this.subline = subline;
  }

  public Double getProgress() {
    return progress;
  }

  public void setProgress(Double progress) {
    this.progress = progress;
  }

  public Boolean getIs_private() {
    return is_private;
  }

  public void setIs_private(Boolean is_private) {
    this.is_private = is_private;
  }

  public void setPrivate(Boolean is_private) {
    this.is_private = is_private;
  }

  public Boolean isPrivate() {
    return is_private;
  }

  public Boolean getProxied_from_ruby() {
    return proxied_from_ruby;
  }

  public void setProxied_from_ruby(Boolean proxied_from_ruby) {
    this.proxied_from_ruby = proxied_from_ruby;
  }

  public String getUserId() {
    return user_id;
  }

  public double getStartedAt() {
    return started_at;
  }

  public String getUserEventId() {
    return user_event_id;
  }
}
