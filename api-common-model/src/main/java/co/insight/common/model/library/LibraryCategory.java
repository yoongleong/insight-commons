package co.insight.common.model.library;

import co.insight.common.model.ObjectUtils;
import co.insight.common.model.common.Picture;
import co.insight.common.model.common.ShareInfo;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class LibraryCategory implements Serializable {
  public static final List<AudioClassifier> interestGroupMcodes = Arrays.asList(
      AudioClassifier.BENEFIT_GROUP,
      AudioClassifier.ORIGIN_GROUP,
      AudioClassifier.PRACTICE_GROUP
  );

  private static final long serialVersionUID = 4301053455613533783L;

  private AudioClassifier mcode;

  private Long vcode;

  private Long parent_vcode;

  private String name;

  private String description;

  private String short_description;

  private String primary_color;

  private String slug;

  private Picture picture;

  private Long picture_version;

  private Picture icon;

  private Long icon_version;

  private Picture rectangle_picture;

  private Long rectangle_picture_version;

  private List<LibraryItemSummary> library_items;

  private Boolean followed_by_me;

  private Boolean followed_privately;

  private Long follower_counts;

  private String public_url;

  private ShareInfo share_info;

  private long cache_version;

  public long getCache_version() {
    return cache_version;
  }

  public void setCache_version(long cache_version) {
    this.cache_version = cache_version;
  }

  public AudioClassifier getMcode() {
    return mcode;
  }

  public void setMcode(AudioClassifier mcode) {
    this.mcode = mcode;
  }

  public Long getVcode() {
    return vcode;
  }

  public void setVcode(Long vcode) {
    this.vcode = vcode;
  }

  public Long getParent_vcode() {
    return parent_vcode;
  }

  public void setParent_vcode(Long parent_vcode) {
    this.parent_vcode = parent_vcode;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getShort_description() {
    return short_description;
  }

  public void setShort_description(String short_description) {
    this.short_description = short_description;
  }

  public String getPrimary_color() {
    return primary_color;
  }

  public void setPrimary_color(String primary_color) {
    this.primary_color = primary_color;
  }

  public String getSlug() {
    return slug;
  }

  public void setSlug(String slug) {
    this.slug = slug;
  }

  public Picture getPicture() {
    return picture;
  }

  public void setPicture(Picture picture) {
    this.picture = picture;
  }

  public Long getPicture_version() {
    return picture_version;
  }

  public void setPicture_version(Long picture_version) {
    this.picture_version = picture_version;
  }

  public Picture getIcon() {
    return icon;
  }

  public void setIcon(Picture icon) {
    this.icon = icon;
  }

  public Long getIcon_version() {
    return icon_version;
  }

  public void setIcon_version(Long icon_version) {
    this.icon_version = icon_version;
  }

  public Long getRectangle_picture_version() {
    return rectangle_picture_version;
  }

  public void setRectangle_picture_version(Long rectangle_picture_version) {
    this.rectangle_picture_version = rectangle_picture_version;
  }

  public Picture getRectangle_picture() {
    return rectangle_picture;
  }

  public void setRectangle_picture(Picture rectangle_picture) {
    this.rectangle_picture = rectangle_picture;
  }

  public List<LibraryItemSummary> getLibrary_items() {
    return library_items;
  }

  public void setLibrary_items(List<LibraryItemSummary> library_items) {
    this.library_items = library_items;
  }

  public Boolean getFollowed_by_me() {
    return followed_by_me;
  }

  public void setFollowed_by_me(Boolean followed_by_me) {
    this.followed_by_me = followed_by_me;
  }

  public Boolean getFollowed_privately() {
    return followed_privately;
  }

  public void setFollowed_privately(Boolean followed_privately) {
    this.followed_privately = followed_privately;
  }

  public boolean isInterestGroup() {
    return mcode != null && interestGroupMcodes.contains(mcode);
  }

  public Long getFollower_counts() {
    return follower_counts;
  }

  public void setFollower_counts(Long follower_counts) {
    this.follower_counts = follower_counts;
  }

  public String getPublic_url() {
    return public_url;
  }

  public void setPublic_url(String public_url) {
    this.public_url = public_url;
  }

  @Override
  public String toString() {
    return "LibraryCategory{" +
        "mcode=" + mcode +
        ", vcode=" + vcode +
        ", parent_vcode=" + parent_vcode +
        ", name='" + name + '\'' +
        ", description='" + description + '\'' +
        ", short_description='" + short_description + '\'' +
        ", primary_color='" + primary_color + '\'' +
        ", slug='" + slug + '\'' +
        ", picture=" + picture +
        ", picture_version=" + picture_version +
        ", icon=" + icon +
        ", icon_version=" + icon_version +
        ", rectangle_picture=" + rectangle_picture +
        ", rectangle_picture_version=" + rectangle_picture_version +
        ", library_items=" + library_items +
        ", followed_by_me=" + followed_by_me +
        ", followed_privately=" + followed_privately +
        ", follower_counts=" + follower_counts +
        ", public_url='" + public_url + '\'' +
        ", share_info=" + share_info +
        ", cache_version=" + cache_version +
        '}';
  }

  public ShareInfo getShare_info() {
    return share_info;
  }

  public void setShare_info(ShareInfo share_info) {
    this.share_info = share_info;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LibraryCategory that = (LibraryCategory) o;
    return mcode == that.mcode &&
        ObjectUtils.equals(vcode, that.vcode) &&
        ObjectUtils.equals(parent_vcode, that.parent_vcode);
  }

  @Override
  public int hashCode() {
    return ObjectUtils.hash(mcode, vcode, parent_vcode);
  }

}
