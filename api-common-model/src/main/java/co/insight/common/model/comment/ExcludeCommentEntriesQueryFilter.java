package co.insight.common.model.comment;

public enum ExcludeCommentEntriesQueryFilter {
  USERS_WITHOUT_PROFILE_PIC,
  LOWER_LEVELS,
  EMPTY_MESSAGE; //exclude comments with empty message
}
