package co.insight.common.model.library;

import co.insight.common.model.audio.AudioItemInfo;
import co.insight.common.model.user.User;

public interface LibraryItemObject {

  String getId();

  void setId(String id);

  User getPublisher();

  void setPublisher(User publisher);

  AudioItemInfo getAudio_item_info();

  void setAudio_item_info(AudioItemInfo audio_item_info);

  Rating getRating_v2();

  void setRating_v2(Rating rating_v2);

}
