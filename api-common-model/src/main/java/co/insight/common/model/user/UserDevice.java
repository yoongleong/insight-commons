package co.insight.common.model.user;

import co.insight.common.model.userdevice.OsType;
import java.io.Serializable;

public class UserDevice implements Serializable {
  private static final long serialVersionUID = 1L;

  private Long id;
  private OsType ost_type;
  private String os_version;
  private String device_name;
  private String device_model;
  private String app_version;
  private String app_build;
  private String device_token;
  private String arn_endpoint;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    UserDevice that = (UserDevice) o;

    if (id != null ? !id.equals(that.id) : that.id != null) {
      return false;
    }
    if (ost_type != that.ost_type) {
      return false;
    }
    if (os_version != null ? !os_version.equals(that.os_version) : that.os_version != null) {
      return false;
    }
    if (device_name != null ? !device_name.equals(that.device_name) : that.device_name != null) {
      return false;
    }
    if (device_model != null ? !device_model.equals(that.device_model) : that.device_model != null) {
      return false;
    }
    if (app_version != null ? !app_version.equals(that.app_version) : that.app_version != null) {
      return false;
    }
    if (app_build != null ? !app_build.equals(that.app_build) : that.app_build != null) {
      return false;
    }
    if (device_token != null ? !device_token.equals(that.device_token) : that.device_token != null) {
      return false;
    }
    return arn_endpoint != null ? arn_endpoint.equals(that.arn_endpoint) : that.arn_endpoint == null;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (ost_type != null ? ost_type.hashCode() : 0);
    result = 31 * result + (os_version != null ? os_version.hashCode() : 0);
    result = 31 * result + (device_name != null ? device_name.hashCode() : 0);
    result = 31 * result + (device_model != null ? device_model.hashCode() : 0);
    result = 31 * result + (app_version != null ? app_version.hashCode() : 0);
    result = 31 * result + (app_build != null ? app_build.hashCode() : 0);
    result = 31 * result + (device_token != null ? device_token.hashCode() : 0);
    result = 31 * result + (arn_endpoint != null ? arn_endpoint.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "UserDevice{" +
        "id=" + id +
        ", ost_type=" + ost_type +
        ", os_version='" + os_version + '\'' +
        ", device_name='" + device_name + '\'' +
        ", device_model='" + device_model + '\'' +
        ", app_version='" + app_version + '\'' +
        ", app_build='" + app_build + '\'' +
        ", device_token='" + device_token + '\'' +
        ", arn_endpoint='" + arn_endpoint + '\'' +
        '}';
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public OsType getOst_type() {
    return ost_type;
  }

  public void setOst_type(OsType ost_type) {
    this.ost_type = ost_type;
  }

  public String getOs_version() {
    return os_version;
  }

  public void setOs_version(String os_version) {
    this.os_version = os_version;
  }

  public String getDevice_name() {
    return device_name;
  }

  public void setDevice_name(String device_name) {
    this.device_name = device_name;
  }

  public String getDevice_model() {
    return device_model;
  }

  public void setDevice_model(String device_model) {
    this.device_model = device_model;
  }

  public String getApp_version() {
    return app_version;
  }

  public void setApp_version(String app_version) {
    this.app_version = app_version;
  }

  public String getApp_build() {
    return app_build;
  }

  public void setApp_build(String app_build) {
    this.app_build = app_build;
  }

  public String getDevice_token() {
    return device_token;
  }

  public void setDevice_token(String device_token) {
    this.device_token = device_token;
  }

  public String getArn_endpoint() {
    return arn_endpoint;
  }

  public void setArn_endpoint(String arn_endpoint) {
    this.arn_endpoint = arn_endpoint;
  }
}
