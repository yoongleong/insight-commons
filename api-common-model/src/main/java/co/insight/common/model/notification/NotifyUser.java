package co.insight.common.model.notification;

public class NotifyUser {

  public static final String MESSAGE_FORMAT= "$publisherHtml has just uploaded <a href=\"$link\">$name</a>";

  private String user_ref_id;
  private String image_url;
  private String image_link;
  private String message;

  public String getUser_ref_id() {
    return user_ref_id;
  }

  public void setUser_ref_id(String user_ref_id) {
    this.user_ref_id = user_ref_id;
  }

  public String getImage_url() {
    return image_url;
  }

  public void setImage_url(String image_url) {
    this.image_url = image_url;
  }

  public String getImage_link() {
    return image_link;
  }

  public void setImage_link(String image_link) {
    this.image_link = image_link;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    NotifyUser that = (NotifyUser) o;

    if (user_ref_id != null ? !user_ref_id.equals(that.user_ref_id) : that.user_ref_id != null) {
      return false;
    }
    if (image_url != null ? !image_url.equals(that.image_url) : that.image_url != null) {
      return false;
    }
    if (image_link != null ? !image_link.equals(that.image_link) : that.image_link != null) {
      return false;
    }
    return message != null ? message.equals(that.message) : that.message == null;
  }

  @Override
  public int hashCode() {
    int result = user_ref_id != null ? user_ref_id.hashCode() : 0;
    result = 31 * result + (image_url != null ? image_url.hashCode() : 0);
    result = 31 * result + (image_link != null ? image_link.hashCode() : 0);
    result = 31 * result + (message != null ? message.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "NotifyUser{" +
        "user_ref_id='" + user_ref_id + '\'' +
        ", image_url='" + image_url + '\'' +
        ", image_link='" + image_link + '\'' +
        ", message='" + message + '\'' +
        '}';
  }
}
