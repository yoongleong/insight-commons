package co.insight.common.model.comment;

import java.io.Serializable;

public class CommentMedia implements Serializable {

  private static final long serialVersionUID = 3461765628841260199L;
  private CommentMediaType type;
  private String file_name;
  private String content_type;
  private Integer file_size;
  private String url;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    CommentMedia that = (CommentMedia) o;

    if (type != that.type) {
      return false;
    }
    if (file_name != null ? !file_name.equals(that.file_name) : that.file_name != null) {
      return false;
    }
    if (content_type != null ? !content_type.equals(that.content_type) : that.content_type != null) {
      return false;
    }
    if (file_size != null ? !file_size.equals(that.file_size) : that.file_size != null) {
      return false;
    }
    return url != null ? url.equals(that.url) : that.url == null;
  }

  @Override
  public int hashCode() {
    int result = type != null ? type.hashCode() : 0;
    result = 31 * result + (file_name != null ? file_name.hashCode() : 0);
    result = 31 * result + (content_type != null ? content_type.hashCode() : 0);
    result = 31 * result + (file_size != null ? file_size.hashCode() : 0);
    result = 31 * result + (url != null ? url.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "CommentMedia{" +
        "type=" + type +
        ", file_name='" + file_name + '\'' +
        ", content_type='" + content_type + '\'' +
        ", file_size=" + file_size +
        ", url='" + url + '\'' +
        '}';
  }

  public CommentMediaType getType() {
    return type;
  }

  public void setType(CommentMediaType type) {
    this.type = type;
  }

  public String getFile_name() {
    return file_name;
  }

  public void setFile_name(String file_name) {
    this.file_name = file_name;
  }

  public String getContent_type() {
    return content_type;
  }

  public void setContent_type(String content_type) {
    this.content_type = content_type;
  }

  public Integer getFile_size() {
    return file_size;
  }

  public void setFile_size(Integer file_size) {
    this.file_size = file_size;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
}
