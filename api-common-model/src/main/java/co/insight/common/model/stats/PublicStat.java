package co.insight.common.model.stats;

public class PublicStat {
  private long u;
  private long t;
  private long g;
  private long rev;

  private long cache_version;

  public long getCache_version() {
    return cache_version;
  }

  public void setCache_version(long cache_version) {
    this.cache_version = cache_version;
  }

  public long getU() {
    return u;
  }

  public void setU(long u) {
    this.u = u;
  }

  public long getT() {
    return t;
  }

  public void setT(long t) {
    this.t = t;
  }

  public long getG() {
    return g;
  }

  public void setG(long g) {
    this.g = g;
  }

  public long getRev() {
    return rev;
  }

  public void setRev(long rev) {
    this.rev = rev;
  }
}
