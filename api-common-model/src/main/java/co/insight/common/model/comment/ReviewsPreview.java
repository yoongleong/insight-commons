package co.insight.common.model.comment;

import co.insight.common.model.library.Rating;
import java.io.Serializable;
import java.util.List;

public class ReviewsPreview implements Serializable {
  private static final long serialVersionUID = 1L;

  private Rating rating;
  private Long comment_count;
  private List<CommentEntry> comments;

  public Rating getRating() {
    return rating;
  }

  public void setRating(Rating rating) {
    this.rating = rating;
  }

  public Long getComment_count() {
    return comment_count;
  }

  public void setComment_count(Long comment_count) {
    this.comment_count = comment_count;
  }

  public List<CommentEntry> getComments() {
    return comments;
  }

  public void setComments(List<CommentEntry> comments) {
    this.comments = comments;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    ReviewsPreview that = (ReviewsPreview) o;

    if (rating != null ? !rating.equals(that.rating) : that.rating != null) {
      return false;
    }
    if (comment_count != null ? !comment_count.equals(that.comment_count) : that.comment_count != null) {
      return false;
    }
    return comments != null ? comments.equals(that.comments) : that.comments == null;
  }

  @Override
  public int hashCode() {
    int result = rating != null ? rating.hashCode() : 0;
    result = 31 * result + (comment_count != null ? comment_count.hashCode() : 0);
    result = 31 * result + (comments != null ? comments.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "ReviewsPreview{" +
        "rating=" + rating +
        ", comment_count=" + comment_count +
        ", comments=" + comments +
        '}';
  }
}
