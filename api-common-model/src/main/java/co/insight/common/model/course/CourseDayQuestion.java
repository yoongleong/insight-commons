package co.insight.common.model.course;

import java.io.Serializable;
import java.util.List;

public class CourseDayQuestion implements Serializable {
  private static final long serialVersionUID = 1L;

  private Long course_day_id;

  private String question;

  private List<CourseDayQoption> options;

  private List<CourseDayQoption> skip_option;

  public Long getCourse_day_id() {
    return course_day_id;
  }

  public void setCourse_day_id(Long course_day_id) {
    this.course_day_id = course_day_id;
  }

  public String getQuestion() {
    return question;
  }

  public void setQuestion(String question) {
    this.question = question;
  }

  public List<CourseDayQoption> getOptions() {
    return options;
  }

  public void setOptions(List<CourseDayQoption> options) {
    this.options = options;
  }

  public List<CourseDayQoption> getSkip_option() {
    return skip_option;
  }

  public void setSkip_option(List<CourseDayQoption> skip_option) {
    this.skip_option = skip_option;
  }
}
