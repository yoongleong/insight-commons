package co.insight.common.model.region;

import java.io.Serializable;

/**
 * Generic data structure for geo point.
 */
public class Point implements Serializable {
  private static final long serialVersionUID = -5147676900986697779L;
  private static final long tenPower6 = Double.valueOf(Math.pow(10, 6)).longValue();

  private Double lat;
  private Double lon;

  public Point() {
  }

  public Point(Double lat, Double lon) {
    this.lat = trimToSixDecimalPlaces(lat);
    this.lon = trimToSixDecimalPlaces(lon);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Point point = (Point) o;

    if (lat != null ? !lat.equals(point.lat) : point.lat != null) {
      return false;
    }
    return lon != null ? lon.equals(point.lon) : point.lon == null;
  }

  @Override
  public int hashCode() {
    int result = lat != null ? lat.hashCode() : 0;
    result = 31 * result + (lon != null ? lon.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Point{" +
        "lat=" + lat +
        ", lon=" + lon +
        '}';
  }

  public Double getLat() {
    return lat;
  }

  public void setLat(Double lat) {
    this.lat = trimToSixDecimalPlaces(lat);
  }

  public Double getLon() {
    return lon;
  }

  public void setLon(Double lon) {
    this.lon = trimToSixDecimalPlaces(lon);
  }

  private Double trimToSixDecimalPlaces(Double d) {
    if (d == null) {
      return null;
    }
    long times10Power6 = Double.valueOf(d.doubleValue() * tenPower6).longValue(); //get rid of decimals
    return Long.valueOf(times10Power6).doubleValue() / tenPower6;
  }

}
