package co.insight.common.model.user;

/**
 * Warning: ordinal (order) matters because db stores it as int.
 * Don't change order.
 */
public enum ExperienceLevel {
  NOT_SPECIFIED, //ordinal = 0
  NO_EXPERIENCE,
  SOME_EXPERIENCE,
  EXPERIENCED,
  TEACHER;
}
