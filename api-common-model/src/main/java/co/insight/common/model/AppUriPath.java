package co.insight.common.model;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.apache.commons.lang3.CharEncoding;
import org.apache.commons.lang3.StringUtils;

/**
 * URI paths for the different app pages
 */
public enum AppUriPath implements Serializable {

  // User page e.g. insight://users/abcd?app_title=Damon
  USER_PAGE(true, "insight://users/"),

  // Teacher page e.g. insight://discoverusers/see_all/followers?userId=xyz
  USER_FOLLOWERS_PAGE(true, "insight://discoverusers/see_all/followers"),

  // Teacher page e.g. insight://discoverusers/see_all/followed_by/xyz
  USER_FOLLOWED_BY_USERID_PAGE(true, "insight://discoverusers/see_all/followed_by"),

  // Group page e.g. insight://groups/abcd?app_title=365+Day+Group
  GROUP_PAGE(true, "insight://groups/"),

  // A single library item e.g. insight://libraries/abcd?app_title=Test+Meditation
  LIBRARY_ITEM_PAGE(true, "insight://libraries/"),

  // Library Explore Page e.g. insight://discoverlibraries/explore?app_title=Explore
  LIBRARY_EXPLORE_PAGE(false, "insight://discoverlibraries/explore"),

  // For you page e.g. insight://discoverlibraries/foryou?app_title=For+you
  LIBRARY_FORYOU_PAGE(false, "insight://discoverlibraries/foryou"), // e.g. insight://libraries/foryou?app_title=For+you

  // My Library Page e.g. insight://discoverlibraries/mylibrary?app_title=My+Library
  LIBRARY_MYLIBRARY_PAGE(false, "insight://discoverlibraries/mylibrary"), // e.g. insight://libraries/mylibrary?app_title=My+Library

  // Generic page e.g. insight://discoverlibraries/pages/popular
  LIBRARY_GENERIC_PAGE(true, "insight://discoverlibraries/pages/"), // e.g. insight://libraries/pages/popular?app_title=Popular

  // Interests Groups Page e.g. insight://discoverlibraries/interest_groups/abcd?app_title=Sleep
  LIBRARY_INTEREST_GROUPS_PAGE(true, "insight://discoverlibraries/interest_groups/"),

  // Interests Page e.g.  // e.g. insight://discoverlibraries/interests/abcd?app_title=Buddhism
  LIBRARY_INTERESTS_PAGE(true, "insight://discoverlibraries/interests/"),

  // Contents Page e.g.  // e.g. insight://discoverlibraries/contents/5001?app_title=Guided
  LIBRARY_CONTENTS_PAGE(true, "insight://discoverlibraries/contents/"),

  // Library See All Page e.g. insight://discoverlibraries/see_all/library_items/abcd?app_title=popular
  LIBRARY_SEE_ALL_LIBRARY_PAGE(false, "insight://discoverlibraries/see_all/library_items/"),

  // Library See All Page e.g. insight://discoverlibraries/see_all/interests/abcd?app_title=sleep
  LIBRARY_SEE_ALL_INTEREST_PAGE(false, "insight://discoverlibraries/see_all/interests/"),

  // Library See All Page e.g. insight://discoverlibraries/see_all/teachers/abcd?app_title=new
  LIBRARY_SEE_ALL_PUBLISHER_PAGE(false, "insight://discoverlibraries/see_all/teachers/"),

  // 365 page e.g. insight://discoverlibraries/365?app_title=365
  LIBRARY_PLAYLIST365_PAGE(false, "insight://discoverlibraries/playlist365"),

  // Library features Page e.g. insight://discoverlibraries/explore?app_title=Features
  LIBRARY_FEATURES_PAGE(false, "insight://discoverlibraries/features"),

  // Library My Downloads Page e.g. insight://discoverlibraries/my_downloads?app_title=My+Downloads
  LIBRARY_MYDOWNLOADS_PAGE(false, "insight://discoverlibraries/my_downloads"),

  // Discover Teachers Page e.g insight://publishers/discover_teachers?app_title=Teachers
  DISCOVER_TEACHERS_PAGE(false, "insight://publishers/discover_teachers"),

  // Teacher page e.g. insight://publishers/teacher/abcd?app_title=Tara+Brach
  TEACHER_PAGE(true, "insight://publishers/teacher/"),

  // Teacher page e.g. insight://publishers/teacher_donation/abcd?app_title=Tara+Brach
  TEACHER_DONATION_PAGE(true, "insight://publishers/teacher_donation/"),

  // Course Explore Page e.g. insight://discovercourses/explore?app_title=Courses
  COURSE_EXPLORE_PAGE(false, "insight://discovercourses/explore"),

  // My Courses Page e.g. insight://discovercourses/myCourses?app_title=MY+COURSES
  COURSE_MYCOURSES_PAGE(false, "insight://discovercourses/mycourses"),

  // Courses See All Page e.g. insight://discovercourses/see_all/courses/abcd?app_title=popular
  COURSE_SEE_ALL_LIBRARY_PAGE(false, "insight://discovercourses/see_all/courses/"),

  // Course page e.g. insight://courses/course_id
  COURSE_PAGE(true, "insight://courses/"),

  UNKNOWN(false, "UNKNOWN"),;

  private boolean requiresAdditionalPath;
  private String value;

  AppUriPath(boolean requiresAdditionalPath, String value) {
    this.requiresAdditionalPath = requiresAdditionalPath;
    this.value = value;
  }

  /**
   * Get enum type with given uri.
   * One Android app uses this to convert a string value (it gets from the user)
   * to an Enum value and then from the Enum value to a server address/path.
   * As long as this function can convert a string value to only one of the Enum values,
   * it is fine.
   *
   * @param value is the target uri.
   * @return the type of this uri.
   */
  public static AppUriPath fromString(String value) {
    if (value == null) {
      return UNKNOWN;
    }
    for (AppUriPath type : values()) {
      if (value.startsWith(type.value)) {
        return type;
      }
    }
    return UNKNOWN;
  }

  public static String getCompleteAppUriPath(AppUriPath uri, String additionalPath, String appTitle) {
    String appTitleParam = "";
    try {
      appTitleParam = "?app_title=" + URLEncoder.encode(appTitle, CharEncoding.UTF_8);
    } catch (UnsupportedEncodingException e) {
      // Ignore - nothing we can do
    }
    if (uri.requiresAdditionalPath) {
      return uri.value + additionalPath + appTitleParam;
    } else {
      return uri.value + appTitleParam;
    }
  }

  public static String getCompleteAppUriPath(AppUriPath uri, String additionalPath, String appTitle, String additionalParams) {
    if (StringUtils.isNotBlank(additionalParams)) {
      return getCompleteAppUriPath(uri, additionalPath, appTitle) + additionalParams;
    }
    return getCompleteAppUriPath(uri, additionalPath, appTitle);
  }

  public static String getLinkForPath(String linkName, String path) {
    String encoded = linkName;
    try {
      encoded = new String(linkName.getBytes(CharEncoding.UTF_8));
    } catch (UnsupportedEncodingException e) {
      // ignore
    }

    return String.format("<a href=\"%s\">%s</a>", path, encoded);
  }

  public boolean isRequiresAdditionalPath() {
    return requiresAdditionalPath;
  }

  public String getValue() {
    return value;
  }
}
