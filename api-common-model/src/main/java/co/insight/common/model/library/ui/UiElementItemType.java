package co.insight.common.model.library.ui;

import java.io.Serializable;

public enum UiElementItemType implements Serializable {
  SQUARE,
  CIRCLE,
  SHIMMER,
}
