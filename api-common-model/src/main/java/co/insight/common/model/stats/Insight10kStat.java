package co.insight.common.model.stats;

import java.io.Serializable;

import co.insight.common.model.user.User;

public class Insight10kStat implements Serializable {
  private static final long serialVersionUID = 1830034170246451562L;
  private String report_date;
  private long total_minutes_all_days;
  private long total_minutes_yesterday;
  private int completed_years;
  private int user_minutes_yesterday;

  private User user;

  private long cache_version;

  public long getCache_version() {
    return cache_version;
  }

  public void setCache_version(long cache_version) {
    this.cache_version = cache_version;
  }

  public String getReport_date() {
    return report_date;
  }

  public void setReport_date(String report_date) {
    this.report_date = report_date;
  }

  public long getTotal_minutes_all_days() {
    return total_minutes_all_days;
  }

  public void setTotal_minutes_all_days(long total_minutes_all_days) {
    this.total_minutes_all_days = total_minutes_all_days;
  }

  public long getTotal_minutes_yesterday() {
    return total_minutes_yesterday;
  }

  public void setTotal_minutes_yesterday(long total_minutes_yesterday) {
    this.total_minutes_yesterday = total_minutes_yesterday;
  }

  public int getCompleted_years() {
    return completed_years;
  }

  public void setCompleted_years(int completed_years) {
    this.completed_years = completed_years;
  }

  public int getUser_minutes_yesterday() {
    return user_minutes_yesterday;
  }

  public void setUser_minutes_yesterday(int user_minutes_yesterday) {
    this.user_minutes_yesterday = user_minutes_yesterday;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
}
