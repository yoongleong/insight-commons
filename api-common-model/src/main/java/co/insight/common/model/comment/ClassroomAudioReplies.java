package co.insight.common.model.comment;

import java.io.Serializable;
import java.util.List;

public class ClassroomAudioReplies implements Serializable {
  /**
   * Default serial version ID.
   */
  private static final long serialVersionUID = 1L;

  private String thread_id;
  private String publisher_first_name;
  private Long comment_count;
  private Long audio_replies_count;
  private List<CommentEntry> audio_replies;

  @Override
  public String toString() {
    return "ClassroomAudioReplies{" +
        "thread_id='" + thread_id + '\'' +
        ", publisher_first_name='" + publisher_first_name + '\'' +
        ", comment_count=" + comment_count +
        ", audio_replies_count=" + audio_replies_count +
        ", audio_replies=" + audio_replies +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    ClassroomAudioReplies that = (ClassroomAudioReplies) o;

    if (thread_id != null ? !thread_id.equals(that.thread_id) : that.thread_id != null) {
      return false;
    }
    if (publisher_first_name != null ? !publisher_first_name.equals(that.publisher_first_name) : that.publisher_first_name != null) {
      return false;
    }
    if (comment_count != null ? !comment_count.equals(that.comment_count) : that.comment_count != null) {
      return false;
    }
    if (audio_replies_count != null ? !audio_replies_count.equals(that.audio_replies_count) : that.audio_replies_count != null) {
      return false;
    }
    return audio_replies != null ? audio_replies.equals(that.audio_replies) : that.audio_replies == null;
  }

  @Override
  public int hashCode() {
    int result = thread_id != null ? thread_id.hashCode() : 0;
    result = 31 * result + (publisher_first_name != null ? publisher_first_name.hashCode() : 0);
    result = 31 * result + (comment_count != null ? comment_count.hashCode() : 0);
    result = 31 * result + (audio_replies_count != null ? audio_replies_count.hashCode() : 0);
    result = 31 * result + (audio_replies != null ? audio_replies.hashCode() : 0);
    return result;
  }

  public String getThread_id() {
    return thread_id;
  }

  public void setThread_id(String thread_id) {
    this.thread_id = thread_id;
  }

  public Long getComment_count() {
    return comment_count;
  }

  public void setComment_count(Long comment_count) {
    this.comment_count = comment_count;
  }

  public Long getAudio_replies_count() {
    return audio_replies_count;
  }

  public void setAudio_replies_count(Long audio_replies_count) {
    this.audio_replies_count = audio_replies_count;
  }

  public List<CommentEntry> getAudio_replies() {
    return audio_replies;
  }

  public void setAudio_replies(List<CommentEntry> audio_replies) {
    this.audio_replies = audio_replies;
  }

  public String getPublisher_first_name() {
    return publisher_first_name;
  }

  public void setPublisher_first_name(String publisher_first_name) {
    this.publisher_first_name = publisher_first_name;
  }
}
