package co.insight.common.model.library;

import java.io.Serializable;

public enum SortFilter implements Serializable {
  MOST_PLAYED,    // sort by played
  HIGHEST_RATED,  // sort by rating
  RECENT_UPLOAD,  // sort by approved date
  DURATION_SHORT_TO_LONG,  // sort by duration
  DURATION_LONG_TO_SHORT,  // sort by duration desc
  ALPHABETICALLY  // sort by name alphabet
}
