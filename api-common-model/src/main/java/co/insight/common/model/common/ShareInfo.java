package co.insight.common.model.common;

import java.io.Serializable;
import java.util.Map;

public class ShareInfo implements Serializable {
  private static final long serialVersionUID = 1L;

  public static final String METADATA_DEEPLINK = "deeplink";
  public static final String METADATA_WEB_URL = "web_url";
  public static final String CONTENT_INDEXING_MODE_PUBLIC = "BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC";

  private String canonical_identifier;
  private String title;
  private String content_description;
  private String content_image_url;
  private String vanity_url;
  private String content_indexing_mode;
  private Map<String, String> metadata;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    ShareInfo shareInfo = (ShareInfo) o;

    if (canonical_identifier != null ? !canonical_identifier.equals(shareInfo.canonical_identifier) : shareInfo.canonical_identifier != null) {
      return false;
    }
    if (title != null ? !title.equals(shareInfo.title) : shareInfo.title != null) {
      return false;
    }
    if (content_description != null ? !content_description.equals(shareInfo.content_description) : shareInfo.content_description != null) {
      return false;
    }
    if (content_image_url != null ? !content_image_url.equals(shareInfo.content_image_url) : shareInfo.content_image_url != null) {
      return false;
    }
    if (vanity_url != null ? !vanity_url.equals(shareInfo.vanity_url) : shareInfo.vanity_url != null) {
      return false;
    }
    if (content_indexing_mode != null ? !content_indexing_mode.equals(shareInfo.content_indexing_mode) : shareInfo.content_indexing_mode != null) {
      return false;
    }
    return metadata != null ? metadata.equals(shareInfo.metadata) : shareInfo.metadata == null;
  }

  @Override
  public int hashCode() {
    int result = canonical_identifier != null ? canonical_identifier.hashCode() : 0;
    result = 31 * result + (title != null ? title.hashCode() : 0);
    result = 31 * result + (content_description != null ? content_description.hashCode() : 0);
    result = 31 * result + (content_image_url != null ? content_image_url.hashCode() : 0);
    result = 31 * result + (vanity_url != null ? vanity_url.hashCode() : 0);
    result = 31 * result + (content_indexing_mode != null ? content_indexing_mode.hashCode() : 0);
    result = 31 * result + (metadata != null ? metadata.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "ShareInfo{" +
        "canonical_identifier='" + canonical_identifier + '\'' +
        ", title='" + title + '\'' +
        ", content_description='" + content_description + '\'' +
        ", content_image_url='" + content_image_url + '\'' +
        ", vanity_url='" + vanity_url + '\'' +
        ", content_indexing_mode='" + content_indexing_mode + '\'' +
        ", metadata=" + metadata +
        '}';
  }

  public String getCanonical_identifier() {
    return canonical_identifier;
  }

  public void setCanonical_identifier(String canonical_identifier) {
    this.canonical_identifier = canonical_identifier;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getContent_description() {
    return content_description;
  }

  public void setContent_description(String content_description) {
    this.content_description = content_description;
  }

  public String getContent_image_url() {
    return content_image_url;
  }

  public void setContent_image_url(String content_image_url) {
    this.content_image_url = content_image_url;
  }

  public String getVanity_url() {
    return vanity_url;
  }

  public void setVanity_url(String vanity_url) {
    this.vanity_url = vanity_url;
  }

  public String getContent_indexing_mode() {
    return content_indexing_mode;
  }

  public void setContent_indexing_mode(String content_indexing_mode) {
    this.content_indexing_mode = content_indexing_mode;
  }

  public Map<String, String> getMetadata() {
    return metadata;
  }

  public void setMetadata(Map<String, String> metadata) {
    this.metadata = metadata;
  }
}
