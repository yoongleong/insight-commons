package co.insight.common.model.common;

import java.io.Serializable;

public class Translation implements Serializable{
  private Language lang;

  private String id;

  public Language getLang() {
    return lang;
  }

  public void setLang(Language lang) {
    this.lang = lang;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
}
