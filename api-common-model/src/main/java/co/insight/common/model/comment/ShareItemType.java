package co.insight.common.model.comment;

import java.io.Serializable;

public enum ShareItemType implements Serializable {
  PUBLISHER,
  LIBRARY_INTEREST,
  LIBRARY_ITEM,
  COURSE_ITEM
}
