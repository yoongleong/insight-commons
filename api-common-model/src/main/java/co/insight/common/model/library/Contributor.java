package co.insight.common.model.library;

import co.insight.common.model.user.User;
import java.io.Serializable;

public class Contributor implements Serializable{
  private static final long serialVersionUID = 3896856389661274915L;
  private Long id;

  private AudioClassifier type;

  private String label;

  private User publisher;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public AudioClassifier getType() {
    return type;
  }

  public void setType(AudioClassifier type) {
    this.type = type;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public User getPublisher() {
    return publisher;
  }

  public void setPublisher(User publisher) {
    this.publisher = publisher;
  }
}
