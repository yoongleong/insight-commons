package co.insight.common.model.course;

import java.io.Serializable;

public class CourseDayAnswer implements Serializable {
  private static final long serialVersionUID = 1L;

  private Long course_day_qoptions_id;

  private Long count;

  private Integer percent;

  public CourseDayAnswer(long qoption, long count) {
    this.course_day_qoptions_id = qoption;
    this.count = count;
  }

  public Long getCourse_day_qoptions_id() {
    return course_day_qoptions_id;
  }

  public void setCourse_day_qoptions_id(Long course_day_qoptions_id) {
    this.course_day_qoptions_id = course_day_qoptions_id;
  }

  public Long getCount() {
    return count;
  }

  public void setCount(Long count) {
    this.count = count;
  }

  public Integer getPercent() {
    return percent;
  }

  public void setPercent(Integer percent) {
    this.percent = percent;
  }
}
