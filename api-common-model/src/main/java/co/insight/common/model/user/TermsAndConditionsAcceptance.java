package co.insight.common.model.user;

import java.io.Serializable;

public class TermsAndConditionsAcceptance implements Serializable {
  private static final long serialVersionUID = 7324116894183170107L;
  private Boolean accept;

  public Boolean getAccept() {
    return accept;
  }

  public void setAccept(Boolean accept) {
    this.accept = accept;
  }
}
