package co.insight.common.model.notification;

import java.io.Serializable;

public class NotificationCount implements Serializable {

  public enum CountOperation {
    SET,      // replace existing value with new one
    INCREMENT, // increment existing value
    RESET      // Reset
  }

  public enum CountType {
    NOTIFICATION,
    MESSAGE,
    FOLLOWER,
    CONNECT
  }

  private static final long serialVersionUID = 1633209311600084145L;

  private Integer newNotificationCount = 0;
  private Integer newMessageCount = 0;
  private Integer newFollowerCount = 0;
  private Integer newConnectCount = 0;

  public Integer getNewNotificationCount() {
    return newNotificationCount;
  }

  public void setNewNotificationCount(Integer newNotificationCount) {
    this.newNotificationCount = newNotificationCount;
  }

  public Integer getNewMessageCount() {
    return newMessageCount;
  }

  public void setNewMessageCount(Integer newMessageCount) {
    this.newMessageCount = newMessageCount;
  }

  public Integer getNewFollowerCount() {
    return newFollowerCount;
  }

  public void setNewFollowerCount(Integer newFollowerCount) {
    this.newFollowerCount = newFollowerCount;
  }

  public Integer getNewConnectCount() {
    return newConnectCount;
  }

  public void setNewConnectCount(Integer newConnectCount) {
    this.newConnectCount = newConnectCount;
  }

  public NotificationCount updateFromCountRequest(CountRequest request) {

    final CountOperation op = request.getOp();
    final CountType type = request.getType();

    switch (type) {
      case FOLLOWER:
        if (op == CountOperation.SET) {
          this.setNewFollowerCount(request.getValue()); // replace value
        } else if (op == CountOperation.INCREMENT) {
          this.setNewFollowerCount(this.getNewFollowerCount() + 1); // increment value
        } else if (op == CountOperation.RESET) {
          this.setNewFollowerCount(0); // reset value
        }
        break;
      case MESSAGE:
        if (op == CountOperation.SET) {
          this.setNewMessageCount(request.getValue());
        } else if (op == CountOperation.INCREMENT) {
          this.setNewMessageCount(this.getNewMessageCount() + 1);
        } else if (op == CountOperation.RESET) {
          this.setNewMessageCount(0);
        }
        break;
      case NOTIFICATION:
        if (op == CountOperation.SET) {
          this.setNewNotificationCount(request.getValue());
        } else if (op == CountOperation.INCREMENT) {
          this.setNewNotificationCount(this.getNewNotificationCount() + 1);
        } else if (op == CountOperation.RESET) {
          this.setNewNotificationCount(0);
        }
        break;
      case CONNECT:
        if (op == CountOperation.SET) {
          this.setNewConnectCount(request.getValue());
        } else if (op == CountOperation.INCREMENT) {
          this.setNewConnectCount(this.getNewConnectCount() + 1);
        } else if (op == CountOperation.RESET) {
          this.setNewConnectCount(0);
        }
        break;
    }

    return this;
  }

}
