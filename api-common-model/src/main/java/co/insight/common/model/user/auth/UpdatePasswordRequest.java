package co.insight.common.model.user.auth;

import java.io.Serializable;

public class UpdatePasswordRequest implements Serializable {

  private static final long serialVersionUID = -6353234302168049992L;
  private String email;
  private String new_password;

  public UpdatePasswordRequest() {
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getNew_password() {
    return new_password;
  }

  public void setNew_password(String newPassword) {
    this.new_password = newPassword;
  }
}
