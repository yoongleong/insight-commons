package co.insight.common.model.user;

import java.io.Serializable;

public class UserSetting implements Serializable{
  private static final long serialVersionUID = 510293919109701828L;
  private Boolean activities_are_private;
  private Boolean dont_show_friends_popup_after_meditation;

  public UserSetting() {
  }

  public UserSetting(Boolean areActivitiesPrivate, Boolean shouldShowMedfriendPopup) {
    this.activities_are_private = areActivitiesPrivate;
    this.dont_show_friends_popup_after_meditation = shouldShowMedfriendPopup;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    UserSetting that = (UserSetting) o;

    if (activities_are_private != null ? !activities_are_private.equals(that.activities_are_private) : that.activities_are_private != null) {
      return false;
    }
    return dont_show_friends_popup_after_meditation != null ? dont_show_friends_popup_after_meditation.equals(that.dont_show_friends_popup_after_meditation) : that.dont_show_friends_popup_after_meditation == null;
  }

  @Override
  public int hashCode() {
    int result = activities_are_private != null ? activities_are_private.hashCode() : 0;
    result = 31 * result + (dont_show_friends_popup_after_meditation != null ? dont_show_friends_popup_after_meditation.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "UserSetting{" +
        "activities_are_private=" + activities_are_private +
        ", dont_show_friends_popup_after_meditation=" + dont_show_friends_popup_after_meditation +
        '}';
  }

  public Boolean getActivities_are_private() {
    return activities_are_private;
  }

  public Boolean areActivitiesPrivate() {
    return activities_are_private;
  }

  public void setActivities_are_private(Boolean activities_are_private) {
    this.activities_are_private = activities_are_private;
  }

  public Boolean getDont_show_friends_popup_after_meditation() {
    return dont_show_friends_popup_after_meditation;
  }

  public void setDont_show_friends_popup_after_meditation(Boolean dont_show_friends_popup_after_meditation) {
    this.dont_show_friends_popup_after_meditation = dont_show_friends_popup_after_meditation;
  }
}
