package co.insight.common.model.branchio;

import java.io.Serializable;

public class BranchIoData implements Serializable {
  private static final long serialVersionUID = 1L;

  private String $canonical_url;
  private String $deeplink;
  private String $desktop_url;
  private String $marketing_title;
  private String $og_app_id;
  private String $og_description;
  private String $og_image_url;
  private String $og_title;
  private String $og_type;
  private String $one_time_use;

  public String get$canonical_url() {
    return $canonical_url;
  }

  public void set$canonical_url(String $canonical_url) {
    this.$canonical_url = $canonical_url;
  }

  public String get$deeplink() {
    return $deeplink;
  }

  public void set$deeplink(String $deeplink) {
    this.$deeplink = $deeplink;
  }

  public String get$desktop_url() {
    return $desktop_url;
  }

  public void set$desktop_url(String $desktop_url) {
    this.$desktop_url = $desktop_url;
  }

  public String get$marketing_title() {
    return $marketing_title;
  }

  public void set$marketing_title(String $marketing_title) {
    this.$marketing_title = $marketing_title;
  }

  public String get$og_app_id() {
    return $og_app_id;
  }

  public void set$og_app_id(String $og_app_id) {
    this.$og_app_id = $og_app_id;
  }

  public String get$og_description() {
    return $og_description;
  }

  public void set$og_description(String $og_description) {
    this.$og_description = $og_description;
  }

  public String get$og_image_url() {
    return $og_image_url;
  }

  public void set$og_image_url(String $og_image_url) {
    this.$og_image_url = $og_image_url;
  }

  public String get$og_title() {
    return $og_title;
  }

  public void set$og_title(String $og_title) {
    this.$og_title = $og_title;
  }

  public String get$og_type() {
    return $og_type;
  }

  public void set$og_type(String $og_type) {
    this.$og_type = $og_type;
  }

  public String get$one_time_use() {
    return $one_time_use;
  }

  public void set$one_time_use(String $one_time_use) {
    this.$one_time_use = $one_time_use;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    BranchIoData that = (BranchIoData) o;

    if ($canonical_url != null ? !$canonical_url.equals(that.$canonical_url) : that.$canonical_url != null) {
      return false;
    }
    if ($deeplink != null ? !$deeplink.equals(that.$deeplink) : that.$deeplink != null) {
      return false;
    }
    if ($desktop_url != null ? !$desktop_url.equals(that.$desktop_url) : that.$desktop_url != null) {
      return false;
    }
    if ($marketing_title != null ? !$marketing_title.equals(that.$marketing_title) : that.$marketing_title != null) {
      return false;
    }
    if ($og_app_id != null ? !$og_app_id.equals(that.$og_app_id) : that.$og_app_id != null) {
      return false;
    }
    if ($og_description != null ? !$og_description.equals(that.$og_description) : that.$og_description != null) {
      return false;
    }
    if ($og_image_url != null ? !$og_image_url.equals(that.$og_image_url) : that.$og_image_url != null) {
      return false;
    }
    if ($og_title != null ? !$og_title.equals(that.$og_title) : that.$og_title != null) {
      return false;
    }
    if ($og_type != null ? !$og_type.equals(that.$og_type) : that.$og_type != null) {
      return false;
    }
    return $one_time_use != null ? $one_time_use.equals(that.$one_time_use) : that.$one_time_use == null;
  }

  @Override
  public int hashCode() {
    int result = $canonical_url != null ? $canonical_url.hashCode() : 0;
    result = 31 * result + ($deeplink != null ? $deeplink.hashCode() : 0);
    result = 31 * result + ($desktop_url != null ? $desktop_url.hashCode() : 0);
    result = 31 * result + ($marketing_title != null ? $marketing_title.hashCode() : 0);
    result = 31 * result + ($og_app_id != null ? $og_app_id.hashCode() : 0);
    result = 31 * result + ($og_description != null ? $og_description.hashCode() : 0);
    result = 31 * result + ($og_image_url != null ? $og_image_url.hashCode() : 0);
    result = 31 * result + ($og_title != null ? $og_title.hashCode() : 0);
    result = 31 * result + ($og_type != null ? $og_type.hashCode() : 0);
    result = 31 * result + ($one_time_use != null ? $one_time_use.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "BranchIoData{" +
        "$canonical_url='" + $canonical_url + '\'' +
        ", $deeplink='" + $deeplink + '\'' +
        ", $desktop_url='" + $desktop_url + '\'' +
        ", $marketing_title='" + $marketing_title + '\'' +
        ", $og_app_id='" + $og_app_id + '\'' +
        ", $og_description='" + $og_description + '\'' +
        ", $og_image_url='" + $og_image_url + '\'' +
        ", $og_title='" + $og_title + '\'' +
        ", $og_type='" + $og_type + '\'' +
        ", $one_time_use='" + $one_time_use + '\'' +
        '}';
  }
}
