package co.insight.common.model.user.auth;

import java.io.Serializable;

public class FirebaseFacebookAuthRequest implements Serializable {

  private static final long serialVersionUID = -1046444876444624456L;

  private String name;
  private String email;
  private String uid;
  private String facebookId;

  public FirebaseFacebookAuthRequest() {
  }

  public FirebaseFacebookAuthRequest(String uid, String facebookId, String email, String name) {
    this.uid = uid;
    this.email = email;
    this.facebookId = facebookId;
    this.name = name;
  }

  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getFacebookId() {
    return facebookId;
  }

  public void setFacebookId(String facebookId) {
    this.facebookId = facebookId;
  }
}
