package co.insight.common.model.library;

public class LibraryUserSettings {

  private Boolean bookmarked;

  private Boolean listen_private;

  private Boolean played_in_full;

  private Boolean repeat_mode;

  private Boolean sleep_mode;

  public Boolean getBookmarked() {
    return bookmarked;
  }

  public void setBookmarked(Boolean bookmarked) {
    this.bookmarked = bookmarked;
  }

  public Boolean getListen_private() {
    return listen_private;
  }

  public void setListen_private(Boolean listen_private) {
    this.listen_private = listen_private;
  }

  public Boolean getPlayed_in_full() {
    return played_in_full;
  }

  public void setPlayed_in_full(Boolean played_in_full) {
    this.played_in_full = played_in_full;
  }

  public Boolean getRepeat_mode() {
    return repeat_mode;
  }

  public void setRepeat_mode(Boolean repeat_mode) {
    this.repeat_mode = repeat_mode;
  }

  public Boolean getSleep_mode() {
    return sleep_mode;
  }

  public void setSleep_mode(Boolean sleep_mode) {
    this.sleep_mode = sleep_mode;
  }
}
