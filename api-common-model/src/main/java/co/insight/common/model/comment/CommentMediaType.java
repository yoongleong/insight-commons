package co.insight.common.model.comment;

public enum CommentMediaType {
  AUDIO,
  IMAGE
}
