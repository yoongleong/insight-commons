package co.insight.common.model.user.auth;

import java.io.Serializable;

public class FirebaseMigrateResponse implements Serializable {

  private static final long serialVersionUID = 6628790217286482978L;

  public enum Type {USER_RECORD, CUSTOM_AUTH}

  private Type type;
  private String user_record;
  private String custom_auth;

  public FirebaseMigrateResponse() {
  }

  public FirebaseMigrateResponse(Type type, String data) {

    this.type = type;

    switch (type) {
      case USER_RECORD:
        this.user_record = data;
        break;
      case CUSTOM_AUTH:
        this.custom_auth = data;
        break;
      default:
        throw new RuntimeException("type not supported");
    }
  }

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }

  public String getUser_record() {
    return user_record;
  }

  public void setUser_record(String user_record) {
    this.user_record = user_record;
  }

  public String getCustom_auth() {
    return custom_auth;
  }

  public void setCustom_auth(String custom_auth) {
    this.custom_auth = custom_auth;
  }
}
