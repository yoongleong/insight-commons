package co.insight.common.model.audio;

import java.io.Serializable;

/**
 * See audio object in documentation page of "Player+screen+API".
 */
public class AudioItemInfo implements Serializable {
  private static final long serialVersionUID = -5152536694018974038L;
  private String id;

  private AudioPlay play;

  private AudioPlay standard_quality_play;

  private Integer play_count;

  private String type;

  private String age;

  private String level;

  private String gender;

  private String contains;

  private String intended_listening;

  private String content;

  private String spiritual;

  private String recording_quality;

  private String release_date;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public AudioPlay getPlay() {
    return play;
  }

  public void setPlay(AudioPlay play) {
    this.play = play;
  }

  public AudioPlay getStandard_quality_play() {
    return standard_quality_play;
  }

  public void setStandard_quality_play(AudioPlay standard_quality_play) {
    this.standard_quality_play = standard_quality_play;
  }

  public Integer getPlay_count() {
    return play_count;
  }

  public void setPlay_count(Integer play_count) {
    this.play_count = play_count;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getAge() {
    return age;
  }

  public void setAge(String age) {
    this.age = age;
  }

  public String getLevel() {
    return level;
  }

  public void setLevel(String level) {
    this.level = level;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getContains() {
    return contains;
  }

  public void setContains(String contains) {
    this.contains = contains;
  }

  public String getIntended_listening() {
    return intended_listening;
  }

  public void setIntended_listening(String intended_listening) {
    this.intended_listening = intended_listening;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getSpiritual() {
    return spiritual;
  }

  public void setSpiritual(String spiritual) {
    this.spiritual = spiritual;
  }

  public String getRecording_quality() {
    return recording_quality;
  }

  public void setRecording_quality(String recording_quality) {
    this.recording_quality = recording_quality;
  }

  public String getRelease_date() {
    return release_date;
  }

  public void setRelease_date(String release_date) {
    this.release_date = release_date;
  }
}
