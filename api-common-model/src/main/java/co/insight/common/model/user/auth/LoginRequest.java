package co.insight.common.model.user.auth;

import java.io.Serializable;

public class LoginRequest implements Serializable {

  private static final long serialVersionUID = -2827887306196880585L;

  private String email;
  private String pwd;

  public LoginRequest() {
  }

  public LoginRequest(String email, String pwd) {
    this.email = email;
    this.pwd = pwd;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPwd() {
    return pwd;
  }

  public void setPwd(String pwd) {
    this.pwd = pwd;
  }
}
