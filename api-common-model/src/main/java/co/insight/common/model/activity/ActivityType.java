package co.insight.common.model.activity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Activity type represents different activity type.
 */
public enum ActivityType implements Serializable {

  /* start of timer meditation types */
  START_MEDITATION("START_MEDITATION"),
  EXTEND_MEDITATION("EXTEND_MEDITATION"),
  END_MEDITATION("END_MEDITATION"),

  START_TAICHI("START_TAICHI"),
  EXTEND_TAICHI("EXTEND_TAICHI"),
  END_TAICHI("END_TAICHI"),

  START_WALKING("START_WALKING"),
  EXTEND_WALKING("EXTEND_WALKING"),
  END_WALKING("END_WALKING"),

  START_BREATHING("START_BREATHING"),
  EXTEND_BREATHING("EXTEND_BREATHING"),
  END_BREATHING("END_BREATHING"),

  START_CHANTING("START_CHANTING"),
  EXTEND_CHANTING("EXTEND_CHANTING"),
  END_CHANTING("END_CHANTING"),

  START_PRAYER("START_PRAYER"),
  EXTEND_PRAYER("EXTEND_PRAYER"),
  END_PRAYER("END_PRAYER"),

  START_HEALING("START_HEALING"),
  EXTEND_HEALING("EXTEND_HEALING"),
  END_HEALING("END_HEALING"),

  START_YOGA("START_YOGA"),
  EXTEND_YOGA("EXTEND_YOGA"),
  END_YOGA("END_YOGA"),
  /* end of timer meditation types */

  /* start of guided meditations */
  START_AUDIO("START_AUDIO"),
  END_AUDIO("END_AUDIO"),

  START_COURSE("START_COURSE"),
  END_COURSE("END_COURSE"),
  /* end of guided meditations */

  DONT_LOG("DONT_LOG"),

  JOIN_GROUP("JOIN_GROUP"),
  POST_IN_GROUP("POST_IN_GROUP"),

  ACCEPT_FRIEND("ACCEPT_FRIEND"),

  UNKNOWN("UNKNOWN"),;

  public static Set<ActivityType> startTypes = new HashSet<>(
      Arrays.asList(
          START_AUDIO,
          START_BREATHING,
          START_CHANTING,
          START_HEALING,
          START_MEDITATION,
          START_PRAYER,
          START_TAICHI,
          START_WALKING,
          START_YOGA,
          START_COURSE
      )
  );

  public static Set<ActivityType> extendingTypes = new HashSet<>(
      Arrays.asList(
          EXTEND_BREATHING,
          EXTEND_CHANTING,
          EXTEND_HEALING,
          EXTEND_MEDITATION,
          EXTEND_PRAYER,
          EXTEND_TAICHI,
          EXTEND_WALKING,
          EXTEND_YOGA
      )
  );

  public static Set<ActivityType> endTypes = new HashSet<>(
      Arrays.asList(
          END_AUDIO,
          END_BREATHING,
          END_CHANTING,
          END_HEALING,
          END_MEDITATION,
          END_PRAYER,
          END_TAICHI,
          END_WALKING,
          END_YOGA,
          END_COURSE
      )
  );

  private String value;

  ActivityType(String value) {
    this.value = value;
  }

  public static ActivityType fromString(String value) {
    for (ActivityType type : values()) {
      if (type.name().equalsIgnoreCase(value)) {
        return type;
      }
    }
    return UNKNOWN;
  }

}
