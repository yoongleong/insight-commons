package co.insight.common.model.course;

import java.io.Serializable;

public class CourseUser implements Serializable {
  private static final long serialVersionUID = 1L;

  private String course_id;

  private String user_id;

  private String current_day_id;

  private Integer current_position;

  private Boolean finished;

  public String getCourse_id() {
    return course_id;
  }

  public void setCourse_id(String course_id) {
    this.course_id = course_id;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public String getCurrent_day_id() {
    return current_day_id;
  }

  public void setCurrent_day_id(String current_day_id) {
    this.current_day_id = current_day_id;
  }

  public Integer getCurrent_position() {
    return current_position;
  }

  public void setCurrent_position(Integer current_position) {
    this.current_position = current_position;
  }

  public Boolean getFinished() {
    return finished;
  }

  public void setFinished(Boolean finished) {
    this.finished = finished;
  }
}
