package co.insight.common.model.library;

import java.io.Serializable;

public enum ContentType implements Serializable {
  GUIDED, //5001
  MUSIC,  //5002
  TALKS   //5003
}
