package co.insight.common.model.user;

import java.io.Serializable;
import java.util.Date;

public class UserStatus implements Serializable {
  private static final long serialVersionUID = 1424975273644462916L;
  public static final long LATEST_CACHE_VERSION = 19l;
  private Long id;
  private Boolean isLocked;
  private Boolean isDeleted;
  private Boolean isBanned;
  private Boolean isGuest;
  private Date createdAt;
  private ExperienceLevel experienceLevel;

  private long cache_version;

  public long getCache_version() {
    return cache_version;
  }

  public void setCache_version(long cache_version) {
    this.cache_version = cache_version;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Boolean getLocked() {
    return isLocked;
  }

  public void setLocked(Boolean locked) {
    isLocked = locked;
  }

  public Boolean getDeleted() {
    return isDeleted;
  }

  public void setDeleted(Boolean deleted) {
    isDeleted = deleted;
  }

  public Boolean getBanned() {
    return isBanned;
  }

  public void setBanned(Boolean banned) {
    isBanned = banned;
  }

  public Boolean getGuest() {
    return isGuest;
  }

  public void setGuest(Boolean guest) {
    isGuest = guest;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public ExperienceLevel getExperienceLevel() {
    return experienceLevel;
  }

  public void setExperienceLevel(ExperienceLevel experienceLevel) {
    this.experienceLevel = experienceLevel;
  }
}
