package co.insight.common.model.library;


import co.insight.common.model.ObjectUtils;
import co.insight.common.model.audio.AudioItemInfo;
import co.insight.common.model.common.Picture;
import co.insight.common.model.user.User;
import java.io.Serializable;
import java.util.List;

public class LibraryItemSummary implements LibraryItemObject, Serializable {
  private static final long serialVersionUID = 79859348868371534L;
  private String id;

  private LibraryType type;

  private String title;

  private String short_description;

  private String public_url;

  private Picture picture;

  private Picture picture_medium;

  private AudioItemInfo audio_item_info;

  private User publisher;

  private String content_type;

  private AudioTag content;

  private Rating rating_v2;

  private Boolean client_show_bookmarked;

  private Boolean bookmarked_by_me;

  private Boolean listening_privately_by_me;

  private Boolean played_in_full_by_me;

  private List<String> for_you_reasons;

  private Boolean is_not_found;

  private Long approved_timestamp;

  private long cache_version;

  private String vanity_url;

  public LibraryItemSummary() {
    super();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LibraryItemSummary that = (LibraryItemSummary) o;
    return ObjectUtils.equals(getId(), that.getId());
  }

  @Override
  public int hashCode() {
    return ObjectUtils.hash(getId(), 31);
  }

  @Override
  public String toString() {
    return "LibraryItemSummary{" +
        "id='" + id + '\'' +
        ", type=" + type +
        ", title='" + title + '\'' +
        ", short_description='" + short_description + '\'' +
        ", public_url='" + public_url + '\'' +
        ", picture=" + picture +
        ", picture_medium=" + picture_medium +
        ", audio_item_info=" + audio_item_info +
        ", publisher=" + publisher +
        ", content_type='" + content_type + '\'' +
        ", content=" + content +
        ", rating_v2=" + rating_v2 +
        ", client_show_bookmarked=" + client_show_bookmarked +
        ", bookmarked_by_me=" + bookmarked_by_me +
        ", listening_privately_by_me=" + listening_privately_by_me +
        ", played_in_full_by_me=" + played_in_full_by_me +
        ", for_you_reasons=" + for_you_reasons +
        ", is_not_found=" + is_not_found +
        ", approved_timestamp=" + approved_timestamp +
        ", vanity_url=" + vanity_url +
        ", cache_version=" + cache_version +
        '}';
  }

  public long getCache_version() {
    return cache_version;
  }

  public void setCache_version(long cache_version) {
    this.cache_version = cache_version;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public LibraryType getType() {
    return type;
  }

  public void setType(LibraryType type) {
    this.type = type;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getShort_description() {
    return short_description;
  }

  public void setShort_description(String short_description) {
    this.short_description = short_description;
  }

  public String getPublic_url() {
    return public_url;
  }

  public void setPublic_url(String public_url) {
    this.public_url = public_url;
  }

  public Picture getPicture() {
    return picture;
  }

  public void setPicture(Picture picture) {
    this.picture = picture;
  }

  public Picture getPicture_medium() {
    return picture_medium;
  }

  public void setPicture_medium(Picture picture_medium) {
    this.picture_medium = picture_medium;
  }

  public AudioItemInfo getAudio_item_info() {
    return audio_item_info;
  }

  public void setAudio_item_info(AudioItemInfo audio_item_info) {
    this.audio_item_info = audio_item_info;
  }

  public User getPublisher() {
    return publisher;
  }

  public void setPublisher(User publisher) {
    this.publisher = publisher;
  }

  public String getContent_type() {
    return content_type;
  }

  public void setContent_type(String content_type) {
    this.content_type = content_type;
  }

  public AudioTag getContent() {
    return content;
  }

  public void setContent(AudioTag content) {
    this.content = content;
  }

  public Rating getRating_v2() {
    return rating_v2;
  }

  public void setRating_v2(Rating rating_v2) {
    this.rating_v2 = rating_v2;
  }

  public Boolean getClient_show_bookmarked() {
    return client_show_bookmarked;
  }

  public void setClient_show_bookmarked(Boolean client_show_bookmarked) {
    this.client_show_bookmarked = client_show_bookmarked;
  }

  public Boolean getBookmarked_by_me() {
    return bookmarked_by_me;
  }

  public void setBookmarked_by_me(Boolean bookmarked_by_me) {
    this.bookmarked_by_me = bookmarked_by_me;
  }

  public Boolean getListening_privately_by_me() {
    return listening_privately_by_me;
  }

  public void setListening_privately_by_me(Boolean listening_privately_by_me) {
    this.listening_privately_by_me = listening_privately_by_me;
  }

  public Boolean getPlayed_in_full_by_me() {
    return played_in_full_by_me;
  }

  public void setPlayed_in_full_by_me(Boolean played_in_full_by_me) {
    this.played_in_full_by_me = played_in_full_by_me;
  }

  public List<String> getFor_you_reasons() {
    return for_you_reasons;
  }

  public void setFor_you_reasons(List<String> for_you_reasons) {
    this.for_you_reasons = for_you_reasons;
  }

  public Boolean getIs_not_found() {
    return is_not_found;
  }

  public void setIs_not_found(Boolean is_not_found) {
    this.is_not_found = is_not_found;
  }

  public Long getApproved_timestamp() {
    return approved_timestamp;
  }

  public void setApproved_timestamp(Long approved_timestamp) {
    this.approved_timestamp = approved_timestamp;
  }

  public String getVanity_url() {
    return vanity_url;
  }

  public void setVanity_url(String vanity_url) {
    this.vanity_url = vanity_url;
  }
}
