package co.insight.common.model.binaryaudio.ffmpeg;

public class Stream {
  private String codec_name;
  private String codec_long_name;

  public String getCodec_name() {
    return codec_name;
  }

  public void setCodec_name(String codec_name) {
    this.codec_name = codec_name;
  }

  public String getCodec_long_name() {
    return codec_long_name;
  }

  public void setCodec_long_name(String codec_long_name) {
    this.codec_long_name = codec_long_name;
  }
}
