package co.insight.common.model.library;

import java.io.Serializable;

public class FilterParams implements Serializable {
  public static final String PARAM_SEP = "&";
  public static final String FIELD_SEP = ",";
  public static final String FIELD_EQ = "=";
  public static final String VALUE_SEP = ":";

  public static final String PARAM_LANG = "lang";

  // query options
  public static final String PARAM_FILTER = "filter";
  public static final String FOLLOWED= "followed";
  public static final String NEW = "new";
  public static final String NEW_TODAY = "new_today";
  public static final String NEW_FOR_YOU = "new_for_you";
  public static final String RECENT = "recent";
  public static final String BOOKMARK = "bookmark";
  public static final String DEFAULT = "default";
  public static final String POPULAR = "popular";
  public static final String CONTRIBUTOR = "contributor";
  public static final String PLAYLIST_365 = "playlist_365";
  public static final String PLAYLIST_BEGINNER = "playlist_beginner";
  public static final String VCODE = "vcode";        // library category (interest / interest group)
  public static final String INTER_VCODE = "inter_vcode";
  public static final String GROUP_VCODE = "group_vcode";
  public static final String MCODE = "mcode";
  public static final String PUBLISHER = "pub_ref";  // publisher by pref_id
  public static final String TERM = "term";          //search term

  public static final String TEACHER = "teacher";
  public static final String RENTED = "rented";
  public static final String PURCHASED = "purchased";


  // sort order
  public static final String PARAM_SORT = "sort";

  public static final String MOST_PLAYED = "played";
  public static final String HIGHEST_RATED = "rated";
  public static final String RECENT_UPLOAD = "recent";
  public static final String DURATION_SHORT_TO_LONG = "dur_short";
  public static final String DURATION_LONG_TO_SHORT = "dur_long";
  public static final String ALPHABETICALLY= "alpha";

  // filter panel
  public static final String PARAM_PANEL = "panel";

  public static final String LENGTH_0_5 = "length_0_5";
  public static final String LENGTH_6_10 = "length_6_10";
  public static final String LENGTH_11_15 = "length_11_15";
  public static final String LENGTH_16_20 = "length_16_20";
  public static final String LENGTH_21_30 = "length_21_30";
  public static final String LENGTH_30_PLUS= "length_30_plus";

  public static final String NO_RELIGIOUS = "no_religious";
  public static final String NO_SPIRITUAL = "no_spiritual";

  public static final String VOICE_MALE = "voice_male";
  public static final String VOICE_FEMALE = "voice_female";

  public static final String BKG_MUSIC_YES = "bkg_music_yes";
  public static final String BKG_MUSIC_NO = "bkg_music_no";
}
