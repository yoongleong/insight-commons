package co.insight.common.model.library;


import co.insight.common.model.common.Language;
import co.insight.common.model.common.ShareInfo;
import co.insight.common.model.common.Translation;
import co.insight.common.model.course.Course;
import co.insight.common.model.region.City;
import java.io.Serializable;
import java.util.List;

public class LibraryItem extends LibraryItemSummary implements LibraryItemObject, Serializable {
  private static final long serialVersionUID = -5851337992052361234L;

  private Course course;

  private List<AudioTag> unhandled_tags;

  private List<AudioTag> other_tags;

  private String long_description;

  private String notes;

  private Language lang;

  private List<Translation> other_langs;

  private List<Contributor> contributors;

  private Rating rating;

  private City region;

  private AudioTag primary_practice;

  private List<AudioTag> secondary_practice;

  private List<AudioTag> other_practice;

  private AudioTag primary_activity;

  private List<AudioTag> secondary_activity;

  private AudioTag primary_origin;

  private List<AudioTag> secondary_origin;

  private List<AudioTag> other_origin;

  private AudioTag primary_benefit;

  private List<AudioTag> secondary_benefit;

  private AudioTag primary_genre;

  private List<AudioTag> secondary_genre;

  private AudioTag formal_practice;

  private AudioTag podcast_format;

  private ShareInfo share_info;

  public List<AudioTag> getUnhandled_tags() {
    return unhandled_tags;
  }

  public void setUnhandled_tags(List<AudioTag> unhandled_tags) {
    this.unhandled_tags = unhandled_tags;
  }

  public List<AudioTag> getOther_tags() {
    return other_tags;
  }

  public void setOther_tags(List<AudioTag> other_tags) {
    this.other_tags = other_tags;
  }

  public String getLong_description() {
    return long_description;
  }

  public void setLong_description(String long_description) {
    this.long_description = long_description;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public Language getLang() {
    return lang;
  }

  public void setLang(Language lang) {
    this.lang = lang;
  }

  public List<Translation> getOther_langs() {
    return other_langs;
  }

  public void setOther_langs(List<Translation> other_langs) {
    this.other_langs = other_langs;
  }

  public List<Contributor> getContributors() {
    return contributors;
  }

  public void setContributors(List<Contributor> contributors) {
    this.contributors = contributors;
  }

  public Rating getRating() {
    return rating;
  }

  public void setRating(Rating rating) {
    this.rating = rating;
  }

  public City getRegion() {
    return region;
  }

  public void setRegion(City region) {
    this.region = region;
  }

  public AudioTag getPrimary_practice() {
    return primary_practice;
  }

  public void setPrimary_practice(AudioTag primary_practice) {
    this.primary_practice = primary_practice;
  }

  public List<AudioTag> getSecondary_practice() {
    return secondary_practice;
  }

  public void setSecondary_practice(List<AudioTag> secondary_practice) {
    this.secondary_practice = secondary_practice;
  }

  public List<AudioTag> getOther_practice() {
    return other_practice;
  }

  public void setOther_practice(List<AudioTag> other_practice) {
    this.other_practice = other_practice;
  }

  public AudioTag getPrimary_activity() {
    return primary_activity;
  }

  public void setPrimary_activity(AudioTag primary_activity) {
    this.primary_activity = primary_activity;
  }

  public List<AudioTag> getSecondary_activity() {
    return secondary_activity;
  }

  public void setSecondary_activity(List<AudioTag> secondary_activity) {
    this.secondary_activity = secondary_activity;
  }

  public AudioTag getPrimary_benefit() {
    return primary_benefit;
  }

  public void setPrimary_benefit(AudioTag primary_benefit) {
    this.primary_benefit = primary_benefit;
  }

  public List<AudioTag> getSecondary_benefit() {
    return secondary_benefit;
  }

  public void setSecondary_benefit(List<AudioTag> secondary_benefit) {
    this.secondary_benefit = secondary_benefit;
  }

  public AudioTag getPrimary_origin() {
    return primary_origin;
  }

  public void setPrimary_origin(AudioTag primary_origin) {
    this.primary_origin = primary_origin;
  }

  public List<AudioTag> getSecondary_origin() {
    return secondary_origin;
  }

  public void setSecondary_origin(List<AudioTag> secondary_origin) {
    this.secondary_origin = secondary_origin;
  }

  public List<AudioTag> getOther_origin() {
    return other_origin;
  }

  public void setOther_origin(List<AudioTag> other_origin) {
    this.other_origin = other_origin;
  }

  public AudioTag getPrimary_genre() {
    return primary_genre;
  }

  public void setPrimary_genre(AudioTag primary_genre) {
    this.primary_genre = primary_genre;
  }

  public List<AudioTag> getSecondary_genre() {
    return secondary_genre;
  }

  public void setSecondary_genre(List<AudioTag> secondary_genre) {
    this.secondary_genre = secondary_genre;
  }

  public AudioTag getFormal_practice() {
    return formal_practice;
  }

  public void setFormal_practice(AudioTag formal_practice) {
    this.formal_practice = formal_practice;
  }

  public AudioTag getPodcast_format() {
    return podcast_format;
  }

  public void setPodcast_format(AudioTag podcast_format) {
    this.podcast_format = podcast_format;
  }

  public ShareInfo getShare_info() {
    return share_info;
  }

  public void setShare_info(ShareInfo share_info) {
    this.share_info = share_info;
  }

  public Course getCourse() {
    return course;
  }

  public void setCourse(Course course) {
    this.course = course;
  }
}
