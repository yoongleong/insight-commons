package co.insight.common.model.co.insight.common.model.payment;

import co.insight.common.model.purchases.PurchaseItemState;
import co.insight.common.model.purchases.PurchaseItemType;
import co.insight.common.model.purchases.PurchasePlatform;
import java.util.Date;

public class PublisherDonation {
  private String purchaseDonationRefId;

  private String user_id;

  private String publisher_id;

  private PurchasePlatform platform;

  private PurchaseItemType type;

  private String items;

  private PurchaseItemState state;

  private Date purchasedAt;

  private Boolean testAccount;

  public String getPurchaseDonationRefId() {
    return purchaseDonationRefId;
  }

  public void setPurchaseDonationRefId(String purchaseDonationRefId) {
    this.purchaseDonationRefId = purchaseDonationRefId;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public String getPublisher_id() {
    return publisher_id;
  }

  public void setPublisher_id(String publisher_id) {
    this.publisher_id = publisher_id;
  }

  public PurchasePlatform getPlatform() {
    return platform;
  }

  public void setPlatform(PurchasePlatform platform) {
    this.platform = platform;
  }

  public PurchaseItemType getType() {
    return type;
  }

  public void setType(PurchaseItemType type) {
    this.type = type;
  }

  public String getItems() {
    return items;
  }

  public void setItems(String items) {
    this.items = items;
  }

  public PurchaseItemState getState() {
    return state;
  }

  public void setState(PurchaseItemState state) {
    this.state = state;
  }

  public Date getPurchasedAt() {
    return purchasedAt;
  }

  public void setPurchasedAt(Date purchasedAt) {
    this.purchasedAt = purchasedAt;
  }

  public Boolean getTestAccount() {
    return testAccount;
  }

  public void setTestAccount(Boolean testAccount) {
    this.testAccount = testAccount;
  }
}
