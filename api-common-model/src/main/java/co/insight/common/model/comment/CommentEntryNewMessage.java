package co.insight.common.model.comment;

import java.io.Serializable;

public class CommentEntryNewMessage implements Serializable{
  private static final long serialVersionUID = 7502309341627425143L;
  private String thread_id;
  private String message;

  public String getThread_id() {
    return thread_id;
  }

  public void setThread_id(String thread_id) {
    this.thread_id = thread_id;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
