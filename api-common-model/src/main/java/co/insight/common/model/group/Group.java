package co.insight.common.model.group;

import co.insight.common.model.common.Picture;
import java.io.Serializable;

public class Group implements Serializable {
  private static final long serialVersionUID = -4705186741338691646L;
  private String id; //this is actually the ref_id, not the "id" column in DB
  private String name;
  private String description;
  private Picture picture;
  private Integer member_count;

  private long cache_version;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Group group = (Group) o;

    if (id != null ? !id.equals(group.id) : group.id != null) {
      return false;
    }
    if (name != null ? !name.equals(group.name) : group.name != null) {
      return false;
    }
    if (description != null ? !description.equals(group.description) : group.description != null) {
      return false;
    }
    return member_count != null ? member_count.equals(group.member_count) : group.member_count == null;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (description != null ? description.hashCode() : 0);
    result = 31 * result + (member_count != null ? member_count.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Group{" +
        "id='" + id + '\'' +
        ", name='" + name + '\'' +
        ", description='" + description + '\'' +
        ", picture=" + picture +
        ", member_count=" + member_count +
        ", cache_version=" + cache_version +
        '}';
  }

  public long getCache_version() {
    return cache_version;
  }

  public void setCache_version(long cache_version) {
    this.cache_version = cache_version;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Picture getPicture() {
    return picture;
  }

  public void setPicture(Picture picture) {
    this.picture = picture;
  }

  public Integer getMember_count() {
    return member_count;
  }

  public void setMember_count(Integer member_count) {
    this.member_count = member_count;
  }
}
