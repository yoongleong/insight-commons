package co.insight.common.model.userdevice;

public enum AwsSnsPlatform {
  APNS,
  APNS_SANDBOX,
  GCM
}
