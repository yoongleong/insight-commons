package co.insight.common.model.user;

import java.io.Serializable;

public class GuestConversionDetails implements Serializable {
  private static final long serialVersionUID = 7424116894183170107L;
  private String name;
  private String email;
  private String pwd;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPwd() {
    return pwd;
  }

  public void setPwd(String pwd) {
    this.pwd = pwd;
  }
}
