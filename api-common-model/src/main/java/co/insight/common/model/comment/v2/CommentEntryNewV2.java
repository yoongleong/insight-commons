package co.insight.common.model.comment.v2;

import co.insight.common.model.comment.CommentEntryMetadata;
import co.insight.common.model.comment.CommentMedia;

/**
 * Payload to create a new comment entry
 */
public class CommentEntryNewV2 implements java.io.Serializable {
  /**
   * Default serial version ID.
   */
  private static final long serialVersionUID = 1L;

  private String thread_id;
  private Integer rating;
  private String message;
  private Boolean is_private;
  private CommentMedia media;
  private CommentEntryMetadata metadata;

  @Override
  public String toString() {
    return "CommentEntryNewV2{" +
        "thread_id='" + thread_id + '\'' +
        ", rating=" + rating +
        ", message='" + message + '\'' +
        ", is_private=" + is_private +
        ", media=" + media +
        ", metadata=" + metadata +
        '}';
  }

  public String getThread_id() {
    return thread_id;
  }

  public void setThread_id(String thread_id) {
    this.thread_id = thread_id;
  }

  public Integer getRating() {
    return rating;
  }

  public void setRating(Integer rating) {
    this.rating = rating;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Boolean getIs_private() {
    return is_private;
  }

  public void setIs_private(Boolean is_private) {
    this.is_private = is_private;
  }

  public CommentMedia getMedia() {
    return media;
  }

  public void setMedia(CommentMedia media) {
    this.media = media;
  }

  public CommentEntryMetadata getMetadata() {
    return metadata;
  }

  public void setMetadata(CommentEntryMetadata metadata) {
    this.metadata = metadata;
  }
}
