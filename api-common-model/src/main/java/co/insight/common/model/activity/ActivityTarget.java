package co.insight.common.model.activity;

import co.insight.common.model.course.Course;
import co.insight.common.model.group.Group;
import co.insight.common.model.library.LibraryItemSummary;
import co.insight.common.model.user.User;
import java.io.Serializable;

public class ActivityTarget implements Serializable {

  private User user;
  private LibraryItemSummary libraryItem;
  private Group group;
  private Course course;

  public ActivityTarget() {
  }

  public ActivityTarget(User user) {
    this.user = user;
  }

  public ActivityTarget(Group group) {
    this.group = group;
  }

  public ActivityTarget(Course course) {
    this.course = course;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    ActivityTarget that = (ActivityTarget) o;

    if (user != null ? !user.equals(that.user) : that.user != null) {
      return false;
    }
    if (libraryItem != null ? !libraryItem.equals(that.libraryItem) : that.libraryItem != null) {
      return false;
    }
    if (group != null ? !group.equals(that.group) : that.group != null) {
      return false;
    }
    return course != null ? course.equals(that.course) : that.course == null;
  }

  @Override
  public int hashCode() {
    int result = user != null ? user.hashCode() : 0;
    result = 31 * result + (libraryItem != null ? libraryItem.hashCode() : 0);
    result = 31 * result + (group != null ? group.hashCode() : 0);
    result = 31 * result + (course != null ? course.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "ActivityTarget{" +
        "user=" + user +
        ", libraryItem=" + libraryItem +
        ", group=" + group +
        ", course=" + course +
        '}';
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Group getGroup() {
    return group;
  }

  public void setGroup(Group group) {
    this.group = group;
  }

  public LibraryItemSummary getLibraryItem() {
    return libraryItem;
  }

  public void setLibraryItem(LibraryItemSummary libraryItem) {
    this.libraryItem = libraryItem;
  }

  public Course getCourse() {
    return course;
  }

  public void setCourse(Course course) {
    this.course = course;
  }
}
