package co.insight.common.model.stats;

public class PublisherStat {
  private Long total_followers;
  private Long new_followers_last_24_hrs;
  private Long new_plays_last_24_hrs;
  private Double avg_rating_last_24_hrs;

  private Long new_followers;
  private Long new_plays;
  private Long ratings;

  public Long getTotal_followers() {
    return total_followers;
  }

  public void setTotal_followers(Long total_followers) {
    this.total_followers = total_followers;
  }

  public Long getNew_followers_last_24_hrs() {
    return new_followers_last_24_hrs;
  }

  public void setNew_followers_last_24_hrs(Long new_followers_last_24_hrs) {
    this.new_followers_last_24_hrs = new_followers_last_24_hrs;
  }

  public Long getNew_plays_last_24_hrs() {
    return new_plays_last_24_hrs;
  }

  public void setNew_plays_last_24_hrs(Long new_plays_last_24_hrs) {
    this.new_plays_last_24_hrs = new_plays_last_24_hrs;
  }

  public Double getAvg_rating_last_24_hrs() {
    return avg_rating_last_24_hrs;
  }

  public void setAvg_rating_last_24_hrs(Double avg_rating_last_24_hrs) {
    this.avg_rating_last_24_hrs = avg_rating_last_24_hrs;
  }

  public Long getNew_followers() {
    return new_followers;
  }

  public void setNew_followers(Long new_followers) {
    this.new_followers = new_followers;
  }

  public Long getNew_plays() {
    return new_plays;
  }

  public void setNew_plays(Long new_plays) {
    this.new_plays = new_plays;
  }

  public Long getRatings() {
    return ratings;
  }

  public void setRatings(Long ratings) {
    this.ratings = ratings;
  }
}
