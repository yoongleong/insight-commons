package co.insight.common.model.comment;

/**
 * Payload to create a new comment entry
 */
public class CommentEntryNew implements java.io.Serializable {
  /**
   * Default serial version ID.
   */
  private static final long serialVersionUID = 1L;

  private String thread_id;

  private String message;

  private Long rating;

  private String target_type;

  private String share;

  private String share_misc;

  private String comment_type;

  private java.util.List<String> targets = new java.util.ArrayList<String>();

  private String share_type;

  /**
   * Returns the value of property "thread_id".
   */
  public String getThread_id() {
    return thread_id;
  }

  /**
   * Updates the value of property "thread_id".
   */
  public void setThread_id(String thread_id) {
    this.thread_id = thread_id;
  }

  /**
   * Returns the value of property "message".
   */
  public String getMessage() {
    return message;
  }

  /**
   * Updates the value of property "message".
   */
  public void setMessage(String message) {
    this.message = message;
  }

  /**
   * Returns the value of property "rating".
   * A rating (for case when thread_id is a legacy comment)
   */
  public Long getRating() {
    return rating;
  }

  /**
   * Updates the value of property "rating".
   */
  public void setRating(Long rating) {
    this.rating = rating;
  }

  /**
   * Returns the value of property "target_type".
   * Where to send the comment e.g. USERS or GROUPS
   */
  public String getTarget_type() {
    return target_type;
  }

  /**
   * Updates the value of property "target_type".
   */
  public void setTarget_type(String target_type) {
    this.target_type = target_type;
  }

  /**
   * Returns the value of property "share".
   * The Id of the object to share
   */
  public String getShare() {
    return share;
  }

  /**
   * Updates the value of property "share".
   */
  public void setShare(String share) {
    this.share = share;
  }

  /**
   * Returns the value of property "share_misc".
   */
  public String getShare_misc() {
    return share_misc;
  }

  /**
   * Updates the value of property "share_misc".
   */
  public void setShare_misc(String share_misc) {
    this.share_misc = share_misc;
  }

  /**
   * Returns the value of property "comment_type".
   * The type of comment
   */
  public String getComment_type() {
    return comment_type;
  }

  /**
   * Updates the value of property "comment_type".
   */
  public void setComment_type(String comment_type) {
    this.comment_type = comment_type;
  }

  /**
   * Returns the value of property "targets".
   * An array of IDs to send the comment to
   */
  public java.util.List<String> getTargets() {
    return targets;
  }

  /**
   * Updates the value of property "targets".
   */
  public void setTargets(java.util.List<String> targets) {
    this.targets = targets;
  }

  /**
   * Returns the value of property "share_type".
   * The type of object to share: PUBLISHER, LIBRARY_ITEM, LIBRARY_INTEREST
   */
  public String getShare_type() {
    return share_type;
  }

  /**
   * Updates the value of property "share_type".
   */
  public void setShare_type(String share_type) {
    this.share_type = share_type;
  }
}
