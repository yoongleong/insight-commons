package co.insight.common.model.common;

import java.io.Serializable;

public class Language implements Serializable {

  private static final long serialVersionUID = -78799678306432372L;
  private String name;

  private String iso_639_1;

  private String icon;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getIso_639_1() {
    return iso_639_1;
  }

  public void setIso_639_1(String iso_639_1) {
    this.iso_639_1 = iso_639_1;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }
}
