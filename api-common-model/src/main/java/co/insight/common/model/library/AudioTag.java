package co.insight.common.model.library;

import java.io.Serializable;

public class AudioTag implements Serializable{
  private static final long serialVersionUID = -6646811767432454435L;
  private Long id;

  private String name;

  private AudioClassifier type;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public AudioClassifier getType() {
    return type;
  }

  public void setType(AudioClassifier type) {
    this.type = type;
  }
}
