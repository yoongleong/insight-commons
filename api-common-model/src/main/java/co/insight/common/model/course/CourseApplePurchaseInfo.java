package co.insight.common.model.course;

import co.insight.common.model.purchases.PurchaseItemType;
import java.io.Serializable;

public class CourseApplePurchaseInfo implements Serializable {
  private static final long serialVersionUID = 1L;

  private String course_id;
  private String purchase_id;
  private PurchaseItemType purchase_item_type;

  public String getCourse_id() {
    return course_id;
  }

  public void setCourse_id(String course_id) {
    this.course_id = course_id;
  }

  public String getPurchase_id() {
    return purchase_id;
  }

  public void setPurchase_id(String purchase_id) {
    this.purchase_id = purchase_id;
  }

  public PurchaseItemType getPurchase_item_type() {
    return purchase_item_type;
  }

  public void setPurchase_item_type(PurchaseItemType purchase_item_type) {
    this.purchase_item_type = purchase_item_type;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    CourseApplePurchaseInfo that = (CourseApplePurchaseInfo) o;

    if (course_id != null ? !course_id.equals(that.course_id) : that.course_id != null) {
      return false;
    }
    if (purchase_id != null ? !purchase_id.equals(that.purchase_id) : that.purchase_id != null) {
      return false;
    }
    return purchase_item_type == that.purchase_item_type;
  }

  @Override
  public int hashCode() {
    int result = course_id != null ? course_id.hashCode() : 0;
    result = 31 * result + (purchase_id != null ? purchase_id.hashCode() : 0);
    result = 31 * result + (purchase_item_type != null ? purchase_item_type.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "CourseApplePurchaseInfo{" +
        "course_id='" + course_id + '\'' +
        ", purchase_id='" + purchase_id + '\'' +
        ", purchase_item_type=" + purchase_item_type +
        '}';
  }
}
