package co.insight.common.model;

import java.io.Serializable;

/**
 * Generic app pages used in {@link AppUriPath#LIBRARY_GENERIC_PAGE}.
 */
public enum AppPages implements Serializable {
  POPULAR("popular"),
  ;

  private String value;

  AppPages(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
