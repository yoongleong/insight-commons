package co.insight.common.model.region;

import java.io.Serializable;

public class City implements Serializable{
  private static final long serialVersionUID = -7462385416032756882L;
  private Long geoname_id;
  private String name;
  private Point location;
  private long cacheVersion;

  public City() {
  }

  public City(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if ((obj == null) || (obj.getClass() != this.getClass())) {
      return false;
    }
    City that = (City) obj;
    return this.getName().equals(that.getName());
  }

  @Override
  public int hashCode() {
    int hash = 7;
    if (getName() != null) {
      hash = 31 * hash + getName().hashCode();
    }
    return hash;
  }

  @Override
  public String toString() {
    return "City{" +
        "geoname_id=" + geoname_id +
        ", name='" + name + '\'' +
        ", location=" + location +
        ", cacheVersion=" + cacheVersion +
        '}';
  }

  public long getCacheVersion() {
    return cacheVersion;
  }

  public void setCacheVersion(long cacheVersion) {
    this.cacheVersion = cacheVersion;
  }

  public Long getGeoname_id() {
    return geoname_id;
  }

  public void setGeoname_id(Long geoname_id) {
    this.geoname_id = geoname_id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Point getLocation() {
    return location;
  }

  public void setLocation(Point location) {
    this.location = location;
  }

}
