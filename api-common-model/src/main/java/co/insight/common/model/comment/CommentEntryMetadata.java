package co.insight.common.model.comment;

import java.io.Serializable;

public class CommentEntryMetadata implements Serializable {

  private static final long serialVersionUID = 6642771003173881670L;

  private String amount_donated;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    CommentEntryMetadata that = (CommentEntryMetadata) o;

    return amount_donated != null ? amount_donated.equals(that.amount_donated) : that.amount_donated == null;
  }

  @Override
  public int hashCode() {
    return amount_donated != null ? amount_donated.hashCode() : 0;
  }

  @Override
  public String toString() {
    return "CommentEntryMetadata{" +
        "amount_donated='" + amount_donated + '\'' +
        '}';
  }

  public String getAmount_donated() {
    return amount_donated;
  }

  public void setAmount_donated(String amount_donated) {
    this.amount_donated = amount_donated;
  }
}
