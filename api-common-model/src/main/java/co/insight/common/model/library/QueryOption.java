package co.insight.common.model.library;

import java.io.Serializable;

public enum QueryOption implements Serializable {
  NEW,
  NEW_TODAY,
  POPULAR
}
