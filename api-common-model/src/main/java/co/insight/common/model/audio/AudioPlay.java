package co.insight.common.model.audio;

import java.io.Serializable;
import java.util.List;

public class AudioPlay implements Serializable{
  private static final long serialVersionUID = -1449052983631355441L;
  private String type;

  private Long length;

  private String path;

  private String path2;

  private String path3;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Long getLength() {
    return length;
  }

  public void setLength(Long length) {
    this.length = length;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public String getPath2() {
    return path2;
  }

  public void setPath2(String path2) {
    this.path2 = path2;
  }

  public String getPath3() {
    return path3;
  }

  public void setPath3(String path3) {
    this.path3 = path3;
  }
}
