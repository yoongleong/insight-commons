package co.insight.common.model.user.auth;

import java.io.Serializable;

public class FirebaseCreateRequest implements Serializable {

  private static final long serialVersionUID = -1046444876444624456L;

  private String name;
  private String email;
  private String pwd;
  private String uid;

  public FirebaseCreateRequest() {
  }

  public FirebaseCreateRequest(String uid, String email, String password, String name) {
    this.uid = uid;
    this.email = email;
    this.pwd = password;
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPwd() {
    return pwd;
  }

  public void setPwd(String pwd) {
    this.pwd = pwd;
  }

  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }
}
