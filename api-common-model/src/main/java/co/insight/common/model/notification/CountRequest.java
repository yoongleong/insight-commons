package co.insight.common.model.notification;

public class CountRequest {

  private static final long serialVersionUID = -3804914986327565355L;

  private NotificationCount.CountType type;
  private NotificationCount.CountOperation op;
  private String userRefId;
  private Integer value;

  public NotificationCount.CountType getType() {
    return type;
  }

  public void setType(NotificationCount.CountType type) {
    this.type = type;
  }

  public NotificationCount.CountOperation getOp() {
    return op;
  }

  public void setOp(NotificationCount.CountOperation op) {
    this.op = op;
  }

  public String getUserRefId() {
    return userRefId;
  }

  public CountRequest setUserRefId(String userRefId) {
    this.userRefId = userRefId;
    return this;
  }

  public Integer getValue() {
    return value;
  }

  public void setValue(Integer value) {
    this.value = value;
  }

  public Boolean isValid() {

    if (op == null) {
      return false;
    }

    switch (op) {
      case INCREMENT:
      case RESET:
        return (type != null);

      case SET:
        return (type != null && value != null);

      default:
        return false;
    }
  }
}
