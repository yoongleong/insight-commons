package co.insight.common.model.notification;

import co.insight.common.model.userdevice.AwsSnsPlatform;
import java.util.Map;

public class PushMessage {
  public static final String DEFAULT_MESSAGE_STRUCTURE = "json";
  public static final String DEFAULT_TITLE = "Insight Timer";
  public static final Integer DEFAULT_BADGE = 0;
  public static final String NOTIFY_SOUND = "n1.mp3";
  public static final String NOTIFY_SOUND_QUIET = "notify_quiet.wav";
  public static final String GROUP_NEW_MESSAGE = "NEW_MESSAGE";
  public static final String CONTENT_DEEPLINK = "CONTENT_DEEPLINK";

  private AwsSnsPlatform platform;
  private String arnEndpoint;
  private String message;
  private String title;
  private String collapseGroup;
  private Metadata meta;

  public AwsSnsPlatform getPlatform() {
    return platform;
  }

  public void setPlatform(AwsSnsPlatform platform) {
    this.platform = platform;
  }

  public String getArnEndpoint() {
    return arnEndpoint;
  }

  public void setArnEndpoint(String arnEndpoint) {
    this.arnEndpoint = arnEndpoint;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getCollapseGroup() {
    return collapseGroup;
  }

  public void setCollapseGroup(String collapseGroup) {
    this.collapseGroup = collapseGroup;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Metadata getMeta() {
    return meta;
  }

  public void setMeta(Metadata meta) {
    this.meta = meta;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder()
        .append("platform:").append(platform == null ? "null" : platform.name()).append(", ")
        .append("arnEndpoint:").append(arnEndpoint).append(", ")
        .append("collapseGroup:").append(collapseGroup).append(", ")
        .append("title:").append(title).append(", ")
        .append("message:").append(message).append(", ")
        .append("meta:").append(meta);

    return sb.toString();
  }

  // Message Metadata
  public static class Metadata {

    private String type;
    private Map<String, Object> data;

    public String getType() {
      return type;
    }

    public void setType(String type) {
      this.type = type;
    }

    public Map<String, Object> getData() {
      return data;
    }

    public void setData(Map<String, Object> data) {
      this.data = data;
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder()
          .append("[type:").append(type).append(", ")
          .append("data:").append(data).append("]");

      return sb.toString();
    }
  }

}
