package co.insight.common.model.common;

import java.io.Serializable;

public class Picture implements Serializable{
  private static final long serialVersionUID = -7360717030271052412L;
  private String type;
  private String path;
  private Long width;
  private Long height;


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Picture picture = (Picture) o;

    if (type != null ? !type.equals(picture.type) : picture.type != null) {
      return false;
    }
    if (path != null ? !path.equals(picture.path) : picture.path != null) {
      return false;
    }
    if (width != null ? !width.equals(picture.width) : picture.width != null) {
      return false;
    }
    return height != null ? height.equals(picture.height) : picture.height == null;
  }

  @Override
  public int hashCode() {
    int result = type != null ? type.hashCode() : 0;
    result = 31 * result + (path != null ? path.hashCode() : 0);
    result = 31 * result + (width != null ? width.hashCode() : 0);
    result = 31 * result + (height != null ? height.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Picture{" +
        "type='" + type + '\'' +
        ", path='" + path + '\'' +
        ", width=" + width +
        ", height=" + height +
        '}';
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public Long getWidth() {
    return width;
  }

  public void setWidth(Long width) {
    this.width = width;
  }

  public Long getHeight() {
    return height;
  }

  public void setHeight(Long height) {
    this.height = height;
  }
}
