package co.insight.common.model.course;

import co.insight.common.model.common.Picture;
import java.io.Serializable;
import java.util.List;

public class CourseDayAnswers implements Serializable {
  private static final long serialVersionUID = 1L;

  private Long course_day_question_id;

  private Long userAnswer;

  private Picture userPicture;

  private Long total_answer_count;

  private List<CourseDayAnswer> answers;

  public Long getCourse_day_question_id() {
    return course_day_question_id;
  }

  public void setCourse_day_question_id(Long course_day_question_id) {
    this.course_day_question_id = course_day_question_id;
  }

  public Long getUserAnswer() {
    return userAnswer;
  }

  public void setUserAnswer(Long userAnswer) {
    this.userAnswer = userAnswer;
  }

  public Picture getUserPicture() {
    return userPicture;
  }

  public void setUserPicture(Picture userPicture) {
    this.userPicture = userPicture;
  }

  public Long getTotal_answer_count() {
    return total_answer_count;
  }

  public void setTotal_answer_count(Long total_answer_count) {
    this.total_answer_count = total_answer_count;
  }

  public List<CourseDayAnswer> getAnswers() {
    return answers;
  }

  public void setAnswers(List<CourseDayAnswer> answers) {
    this.answers = answers;
  }
}
