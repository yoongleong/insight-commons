package co.insight.common.model.library.ui;

import java.io.Serializable;

public enum UiElementContentType implements Serializable {
  LABEL_WITH_PICTURE,
  INTEREST,
  INTEREST_CONTENT,
  INTEREST_GROUP, //for anything that has to do with non-library and non-publisher carousels, vertical lists
  PRACTICES, // e.g. practices list in explore page
  TAGS, // for tags (e.g. popular tags in explore page)
  LABEL_WITH_COUNTER, // e.g. in main nav list in explore page
  TIME_FILTER, //time buckets
  PLAYLIST,
  LIBRARY_ITEM,
  PUBLISHER,
  PUBLISHERS,
  USER_INFO,
  NONE,
  GENERIC_ITEM,
  SEARCH_TOP_RESULT,
  COURSES,
  ;
}
