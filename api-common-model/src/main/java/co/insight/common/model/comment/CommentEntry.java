package co.insight.common.model.comment;

import co.insight.common.model.ObjectUtils;
import co.insight.common.model.user.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CommentEntry implements Serializable {
  private static final long serialVersionUID = 8007609221555690271L;
  private String id;
  private String thread_id;
  private String parent_id;
  private String item_id;
  private User owner;
  private Integer likes;
  private Integer reported;
  private String message;
  private Integer rating;
  private List<CommentReply> replies = new ArrayList<>();
  private List<CommentReply> audio_replies = new ArrayList<>();
  private Date created_at;
  private Date updated_at;
  private Boolean is_mine;
  private Boolean is_private;
  private CommentMedia media;
  private CommentEntryMetadata metadata;
  private Long number_of_replies;
  private Long number_of_audio_replies;
  private Integer likes_count;
  private Integer flagged_count;
  private long cache_version;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CommentEntry entry = (CommentEntry) o;
    return ObjectUtils.equals(id, entry.id) &&
        ObjectUtils.equals(thread_id, entry.thread_id) &&
        ObjectUtils.equals(item_id, entry.item_id);
  }

  @Override
  public int hashCode() {
    return ObjectUtils.hash(id, thread_id, item_id);
  }

  @Override
  public String toString() {
    return "CommentEntry{" +
        "id='" + id + '\'' +
        ", thread_id='" + thread_id + '\'' +
        ", parent_id='" + parent_id + '\'' +
        ", item_id='" + item_id + '\'' +
        ", owner=" + owner +
        ", likes=" + likes +
        ", reported=" + reported +
        ", message='" + message + '\'' +
        ", rating=" + rating +
        ", created_at=" + created_at +
        ", updated_at=" + updated_at +
        ", is_mine=" + is_mine +
        ", is_private=" + is_private +
        ", media=" + media +
        ", metadata=" + metadata +
        ", number_of_replies=" + number_of_replies +
        ", number_of_audio_replies=" + number_of_audio_replies +
        ", likes_count=" + likes_count +
        ", flagged_count=" + flagged_count +
        ", cache_version=" + cache_version +
        '}';
  }

  public long getCache_version() {
    return cache_version;
  }

  public void setCache_version(long cache_version) {
    this.cache_version = cache_version;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getThread_id() {
    return thread_id;
  }

  public void setThread_id(String thread_id) {
    this.thread_id = thread_id;
  }

  public String getParent_id() {
    return parent_id;
  }

  public void setParent_id(String parent_id) {
    this.parent_id = parent_id;
  }

  public String getItem_id() {
    return item_id;
  }

  public void setItem_id(String item_id) {
    this.item_id = item_id;
  }

  public User getOwner() {
    return owner;
  }

  public void setOwner(User owner) {
    this.owner = owner;
  }

  public Integer getLikes() {
    return likes;
  }

  public void setLikes(Integer likes) {
    this.likes = likes;
  }

  public Integer getReported() {
    return reported;
  }

  public void setReported(Integer reported) {
    this.reported = reported;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Integer getRating() {
    return rating;
  }

  public void setRating(Integer rating) {
    this.rating = rating;
  }

  public List<CommentReply> getReplies() {
    return replies;
  }

  public void setReplies(List<CommentReply> replies) {
    this.replies = replies;
  }

  public List<CommentReply> getAudioReplies() {
    return audio_replies;
  }

  public void setAudioReplies(List<CommentReply> audio_replies) {
    this.audio_replies = audio_replies;
  }

  public Date getCreated_at() {
    return created_at;
  }

  public void setCreated_at(Date created_at) {
    this.created_at = created_at;
  }

  public Date getUpdated_at() {
    return updated_at;
  }

  public void setUpdated_at(Date updated_at) {
    this.updated_at = updated_at;
  }

  public Boolean getIs_mine() {
    return is_mine;
  }

  public void setIs_mine(Boolean is_mine) {
    this.is_mine = is_mine;
  }

  public Boolean getIs_private() {
    return is_private;
  }

  public void setIs_private(Boolean is_private) {
    this.is_private = is_private;
  }

  public CommentMedia getMedia() {
    return media;
  }

  public void setMedia(CommentMedia media) {
    this.media = media;
  }

  public CommentEntryMetadata getMetadata() {
    return metadata;
  }

  public void setMetadata(CommentEntryMetadata metadata) {
    this.metadata = metadata;
  }

  public Long getNumber_of_replies() {
    return number_of_replies;
  }

  public void setNumber_of_replies(Long number_of_replies) {
    this.number_of_replies = number_of_replies;
  }

  public Integer getLikes_count() {
    return likes_count;
  }

  public void setLikes_count(Integer likes_count) {
    this.likes_count = likes_count;
  }

  public Integer getFlagged_count() {
    return flagged_count;
  }

  public void setFlagged_count(Integer flagged_count) {
    this.flagged_count = flagged_count;
  }

  public Long getNumber_of_audio_replies() {
    return number_of_audio_replies;
  }

  public void setNumber_of_audio_replies(Long numberOfAudioReplies) {
    this.number_of_audio_replies = numberOfAudioReplies;
  }
}
