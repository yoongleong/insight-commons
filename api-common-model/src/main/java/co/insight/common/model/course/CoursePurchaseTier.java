package co.insight.common.model.course;

public enum CoursePurchaseTier {
  INSIGHT_FREE_COURSE,
  INSIGHT_10_DAY_COURSE,
  INSIGHT_30_DAY_COURSE,
}
