package co.insight.common.model.library.ui;

import java.io.Serializable;

public class UiElementItem implements Serializable {
  private static final long serialVersionUID = 1L;

  private String title;

  private String uri;

  private String picture_url;

  private Integer count;

  private String description;

  private String follow_id;

  private Long follow_count;

  private Boolean followed_by_me;

  private UiElementItemType item_type;

  private Boolean is_new;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getUri() {
    return uri;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }

  public String getPicture_url() {
    return picture_url;
  }

  public void setPicture_url(String picture_url) {
    this.picture_url = picture_url;
  }

  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getFollow_id() {
    return follow_id;
  }

  public void setFollow_id(String follow_id) {
    this.follow_id = follow_id;
  }

  public Long getFollow_count() {
    return follow_count;
  }

  public void setFollow_count(Long follow_count) {
    this.follow_count = follow_count;
  }

  public Boolean getFollowed_by_me() {
    return followed_by_me;
  }

  public void setFollowed_by_me(Boolean followed_by_me) {
    this.followed_by_me = followed_by_me;
  }

  public UiElementItemType getItem_type() {
    return item_type;
  }

  public void setItem_type(UiElementItemType item_type) {
    this.item_type = item_type;
  }

  public Boolean getIs_new() {
    return is_new;
  }

  public void setIs_new(Boolean is_new) {
    this.is_new = is_new;
  }
}
