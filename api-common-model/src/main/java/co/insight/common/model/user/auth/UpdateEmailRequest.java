package co.insight.common.model.user.auth;

import java.io.Serializable;

public class UpdateEmailRequest implements Serializable {

  private String email;
  private String new_email;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getNew_email() {
    return new_email;
  }

  public void setNew_email(String newEmail) {
    this.new_email = newEmail;
  }
}
