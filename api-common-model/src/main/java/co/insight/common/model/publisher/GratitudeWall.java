package co.insight.common.model.publisher;

import java.io.Serializable;

public class GratitudeWall implements Serializable {
  private static final long serialVersionUID = 1L;

  private String gratitude_wall;

  public String getGratitude_wall() {
    return gratitude_wall;
  }

  public void setGratitude_wall(String gratitude_wall) {
    this.gratitude_wall = gratitude_wall;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    GratitudeWall that = (GratitudeWall) o;

    return gratitude_wall != null ? gratitude_wall.equals(that.gratitude_wall) : that.gratitude_wall == null;
  }

  @Override
  public int hashCode() {
    return gratitude_wall != null ? gratitude_wall.hashCode() : 0;
  }

  @Override
  public String toString() {
    return "GratitudeWall{" +
        "gratitude_wall='" + gratitude_wall + '\'' +
        '}';
  }
}
