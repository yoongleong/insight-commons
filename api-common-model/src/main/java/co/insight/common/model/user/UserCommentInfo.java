package co.insight.common.model.user;

import java.io.Serializable;

public class UserCommentInfo implements Serializable {
  private static final long serialVersionUID = 1L;
  private String commentEntryId;
  private Boolean liked;
  private Boolean flagged;
  private Boolean replied;

  @Override
  public String toString() {
    return "UserCommentInfo{" +
        "commentEntryId='" + commentEntryId + '\'' +
        ", liked=" + liked +
        ", flagged=" + flagged +
        ", replied=" + replied +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    UserCommentInfo that = (UserCommentInfo) o;

    if (commentEntryId != null ? !commentEntryId.equals(that.commentEntryId) : that.commentEntryId != null) {
      return false;
    }
    if (liked != null ? !liked.equals(that.liked) : that.liked != null) {
      return false;
    }
    if (flagged != null ? !flagged.equals(that.flagged) : that.flagged != null) {
      return false;
    }
    return replied != null ? replied.equals(that.replied) : that.replied == null;
  }

  @Override
  public int hashCode() {
    int result = commentEntryId != null ? commentEntryId.hashCode() : 0;
    result = 31 * result + (liked != null ? liked.hashCode() : 0);
    result = 31 * result + (flagged != null ? flagged.hashCode() : 0);
    result = 31 * result + (replied != null ? replied.hashCode() : 0);
    return result;
  }

  public String getCommentEntryId() {
    return commentEntryId;
  }

  public void setCommentEntryId(String commentEntryId) {
    this.commentEntryId = commentEntryId;
  }

  public Boolean getLiked() {
    return liked;
  }

  public void setLiked(Boolean liked) {
    this.liked = liked;
  }

  public Boolean getFlagged() {
    return flagged;
  }

  public void setFlagged(Boolean flagged) {
    this.flagged = flagged;
  }

  public Boolean getReplied() {
    return replied;
  }

  public void setReplied(Boolean replied) {
    this.replied = replied;
  }
}
