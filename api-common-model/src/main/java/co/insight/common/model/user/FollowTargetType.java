package co.insight.common.model.user;

public enum FollowTargetType {
  INTEREST,
  TEACHER; //In the near-future we will have other types
}

