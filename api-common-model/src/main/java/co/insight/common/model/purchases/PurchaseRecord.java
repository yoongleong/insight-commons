package co.insight.common.model.purchases;

import static co.insight.common.model.purchases.PurchaseItemType.COURSE_BALANCE_10_DAY;
import static co.insight.common.model.purchases.PurchaseItemType.COURSE_BALANCE_30_DAY;
import static co.insight.common.model.purchases.PurchaseItemType.COURSE_FREE;
import static co.insight.common.model.purchases.PurchaseItemType.COURSE_PURCHASE_10_DAY;
import static co.insight.common.model.purchases.PurchaseItemType.COURSE_PURCHASE_30_DAY;
import static co.insight.common.model.purchases.PurchaseItemType.COURSE_RENTAL_10_DAY;
import static co.insight.common.model.purchases.PurchaseItemType.COURSE_RENTAL_30_DAY;
import static co.insight.common.model.purchases.PurchaseItemType.MELCHIOR_1;
import static co.insight.common.model.purchases.PurchaseItemType.MELCHIOR_2;
import static co.insight.common.model.purchases.PurchaseItemType.MELCHIOR_3;

import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;

public class PurchaseRecord {

  private Integer id;
  private PurchasePlatform platform;
  private String receipt;
  private String receipt_hash;
  private PurchaseItemType type;
  private List<String> packs;
  private PurchaseItemState state;
  private Date purchase_date;
  private Date expiry_date;
  private Date validate_date;
  private Boolean test_account;

  public static final EnumSet<PurchaseItemType> COURSES = EnumSet.of(
      COURSE_FREE,
      COURSE_RENTAL_10_DAY,
      COURSE_PURCHASE_10_DAY,
      COURSE_BALANCE_10_DAY,
      COURSE_RENTAL_30_DAY,
      COURSE_PURCHASE_30_DAY,
      COURSE_BALANCE_30_DAY);

  public static final EnumSet<PurchaseItemType> MELCHIOR = EnumSet.of(
      MELCHIOR_1,
      MELCHIOR_2,
      MELCHIOR_3);

  public static final EnumSet<PurchaseItemType> COURSE_RENTALS = EnumSet.of(
      COURSE_RENTAL_10_DAY,
      COURSE_RENTAL_30_DAY);

  public static final EnumSet<PurchaseItemType> COURSE_UPGRADES = EnumSet.of(
      COURSE_BALANCE_10_DAY,
      COURSE_BALANCE_30_DAY);

  public static final EnumSet<PurchaseItemType> COURSE_OWNED = EnumSet.of(
      COURSE_FREE,
      COURSE_BALANCE_10_DAY,
      COURSE_PURCHASE_10_DAY,
      COURSE_BALANCE_30_DAY,
      COURSE_PURCHASE_30_DAY);

  public PurchaseRecord() {
  }

  public PurchaseRecord(Integer id, PurchasePlatform platform, String receipt, String receipt_hash, PurchaseItemType type, List<String> packs,
                        PurchaseItemState state, Date purchase_date, Date expiry_date, Date validate_date, Boolean test_account) {
    this.id = id;
    this.platform = platform;
    this.receipt = receipt;
    this.receipt_hash = receipt_hash;
    this.type = type;
    this.packs = packs;
    this.state = state;
    this.purchase_date = purchase_date;
    this.expiry_date = expiry_date;
    this.validate_date = validate_date;
    this.test_account = test_account;
  }

  public void setPacks(List<String> packs) {
    Objects.requireNonNull(packs, "Purchase packs cannot be null");
    this.packs = packs;
  }

  public void setState(PurchaseItemState state) {
    Objects.requireNonNull(state, "PurchaseItemState cannot be null");
    this.state = state;
  }

  public void setPurchase_date(Date date) {
    Objects.requireNonNull(date, "Purchase date cannot be null");
    this.purchase_date = date;
  }

  public Integer getId() { return id; }

  public void setId(Integer id) { this.id = id; }

  public void setExpiry_date(Date expiry_date) {
    this.expiry_date = expiry_date;
  }

  public void setPlatform(PurchasePlatform platform) {
    this.platform = platform;
  }

  public void setReceipt(String receipt) {
    this.receipt = receipt;
  }

  public void setType(PurchaseItemType type) {
    this.type = type;
  }

  public String getReceipt_hash() { return receipt_hash; }

  public void setReceipt_hash(String receipt_hash) { this.receipt_hash = receipt_hash; }

  public PurchasePlatform getPlatform() {
    return platform;
  }

  public String getReceipt() {
    return receipt;
  }

  public PurchaseItemType getType() {
    return type;
  }

  public List<String> getPacks() {
    return packs;
  }

  public PurchaseItemState getState() {
    return state;
  }

  public Date getPurchase_date() {
    return purchase_date;
  }

  public Date getExpiry_date() {
    return expiry_date;
  }

  public Date getValidate_date() { return validate_date; }

  public void setValidate_date(Date validate_date) { this.validate_date = validate_date; }

  public Boolean getTest_account() { return test_account; }

  public void setTest_account(Boolean test_account) { this.test_account = test_account; }

  /**
   * TODO: maybe need to remove packs from uniqueness.  To cater for the case where items are changed in the future.
   * <p>
   * <li>courses - each receipt will tie to a item (one-to-one)</li>
   * <li>subscriptions - items may change over time</li>
   * </p>
   */
  @Override
  public boolean equals(Object obj) {
    if(this==obj) {
      return true;
    }
    if (obj == null || this.getClass() != obj.getClass()) {
      return false;
    }

    PurchaseRecord other = (PurchaseRecord) obj;
    return platform == other.getPlatform() &&
        receipt_hash.contentEquals(other.getReceipt_hash()) &&
        type == other.getType();
  }

  @Override
  public int hashCode() {
    return Objects.hash(platform, receipt_hash, type);
  }

}
