package co.insight.common.model.library;

import co.insight.common.model.common.Picture;
import java.io.Serializable;

public class Playlist implements Serializable {
  private Long id;

  private String name;

  private String short_name;

  private String description;

  private boolean featured;

  private Long total_tracks;

  private Long total_views;

  private Picture picture;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getShort_name() {
    return short_name;
  }

  public void setShort_name(String short_name) {
    this.short_name = short_name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public boolean getFeatured() {
    return featured;
  }

  public void setFeatured(boolean featured) {
    this.featured = featured;
  }

  public Long getTotal_tracks() {
    return total_tracks;
  }

  public void setTotal_tracks(Long total_tracks) {
    this.total_tracks = total_tracks;
  }

  public Long getTotal_views() {
    return total_views;
  }

  public void setTotal_views(Long total_views) {
    this.total_views = total_views;
  }

  public Picture getPicture() {
    return picture;
  }

  public void setPicture(Picture picture) {
    this.picture = picture;
  }
}
