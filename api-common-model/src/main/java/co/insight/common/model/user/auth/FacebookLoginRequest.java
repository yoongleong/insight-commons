package co.insight.common.model.user.auth;

import java.io.Serializable;

public class FacebookLoginRequest implements Serializable {

  private static final long serialVersionUID = -1935099006467124892L;
  private String name;
  private String email;
  /**
   * facebook id.
   */
  private String fb_id;
  /**
   * facebook token.
   */
  private String fb_ref_id;
  private String picture_url;


  public FacebookLoginRequest() {
  }

  public FacebookLoginRequest(String fb_id, String fb_ref_id, String email, String name) {
    this.email = email;
    this.fb_id = fb_id;
    this.fb_ref_id = fb_ref_id;
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getFb_id() {
    return fb_id;
  }

  public void setFb_id(String fb_id) {
    this.fb_id = fb_id;
  }

  public String getFb_ref_id() {
    return fb_ref_id;
  }

  public void setFb_ref_id(String fb_ref_id) {
    this.fb_ref_id = fb_ref_id;
  }

  public String getPicture_url() {
    return picture_url;
  }

  public void setPicture_url(String picture_url) {
    this.picture_url = picture_url;
  }

}
