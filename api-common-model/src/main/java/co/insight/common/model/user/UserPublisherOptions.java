package co.insight.common.model.user;

import java.io.Serializable;

public class UserPublisherOptions implements Serializable {

  private Boolean allow_notifications;

  private Boolean following;

  private Boolean following_privately;

  private Boolean allow_messaging;

  public Boolean getAllow_notifications() {
    return allow_notifications;
  }

  public void setAllow_notifications(Boolean allow_notifications) {
    this.allow_notifications = allow_notifications;
  }

  public Boolean getAllow_messaging() {
    return allow_messaging;
  }

  public void setAllow_messaging(Boolean allow_messaging) {
    this.allow_messaging = allow_messaging;
  }

  public Boolean getFollowing() {
    return following;
  }

  public void setFollowing(Boolean following) {
    this.following = following;
  }

  public Boolean getFollowing_privately() {
    return following_privately;
  }

  public void setFollowing_privately(Boolean following_privately) {
    this.following_privately = following_privately;
  }

  @Override
  public String toString() {
    return "UserPublisherOptions{" +
        "allow_notifications=" + allow_notifications +
        ", following=" + following +
        ", following_privately=" + following_privately +
        ", allow_messaging=" + allow_messaging +
        '}';
  }
}
