package co.insight.common.model.comment;

import co.insight.common.model.user.User;
import java.io.Serializable;
import java.util.Date;

public class CommentReply implements Serializable {
  private static final long serialVersionUID = -6130355570448483664L;
  private String entry_id;
  private User owner;
  private String message;
  private CommentMedia media;
  private Date created_at;
  private Date updated_at;
  private Integer likes_count;
  private Integer flagged_count;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    CommentReply that = (CommentReply) o;

    if (entry_id != null ? !entry_id.equals(that.entry_id) : that.entry_id != null) {
      return false;
    }
    if (owner != null ? !owner.equals(that.owner) : that.owner != null) {
      return false;
    }
    if (message != null ? !message.equals(that.message) : that.message != null) {
      return false;
    }
    if (media != null ? !media.equals(that.media) : that.media != null) {
      return false;
    }
    if (likes_count != null ? !likes_count.equals(that.likes_count) : that.likes_count != null) {
      return false;
    }
    return flagged_count != null ? flagged_count.equals(that.flagged_count) : that.flagged_count == null;
  }

  @Override
  public int hashCode() {
    int result = entry_id != null ? entry_id.hashCode() : 0;
    result = 31 * result + (owner != null ? owner.hashCode() : 0);
    result = 31 * result + (message != null ? message.hashCode() : 0);
    result = 31 * result + (media != null ? media.hashCode() : 0);
    result = 31 * result + (created_at != null ? created_at.hashCode() : 0);
    result = 31 * result + (updated_at != null ? updated_at.hashCode() : 0);
    result = 31 * result + (likes_count != null ? likes_count.hashCode() : 0);
    result = 31 * result + (flagged_count != null ? flagged_count.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "CommentReply{" +
        "entry_id='" + entry_id + '\'' +
        ", owner=" + owner +
        ", message='" + message + '\'' +
        ", media=" + media +
        ", created_at=" + created_at +
        ", updated_at=" + updated_at +
        ", likes_count=" + likes_count +
        ", flagged_count=" + flagged_count +
        '}';
  }

  public String getEntry_id() {
    return entry_id;
  }

  public void setEntry_id(String entry_id) {
    this.entry_id = entry_id;
  }

  public User getOwner() {
    return owner;
  }

  public void setOwner(User owner) {
    this.owner = owner;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public CommentMedia getMedia() {
    return media;
  }

  public void setMedia(CommentMedia media) {
    this.media = media;
  }

  public Date getCreated_at() {
    return created_at;
  }

  public void setCreated_at(Date created_at) {
    this.created_at = created_at;
  }

  public Date getUpdated_at() {
    return updated_at;
  }

  public void setUpdated_at(Date updated_at) {
    this.updated_at = updated_at;
  }

  public Integer getLikes_count() {
    return likes_count;
  }

  public void setLikes_count(Integer likes_count) {
    this.likes_count = likes_count;
  }

  public Integer getFlagged_count() {
    return flagged_count;
  }

  public void setFlagged_count(Integer flagged_count) {
    this.flagged_count = flagged_count;
  }
}
