package co.insight.common.model.binaryaudio.ffmpeg;

import java.util.List;

public class FfprobeOutput {

  private List<Stream> streams;

  public List<Stream> getStreams() {
    return streams;
  }

  public void setStreams(List<Stream> streams) {
    this.streams = streams;
  }
}
