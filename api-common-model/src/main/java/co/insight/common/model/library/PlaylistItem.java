package co.insight.common.model.library;

import java.io.Serializable;

public class PlaylistItem implements Serializable {
  private Long id;
  private Long playlist_id;
  private String library_ref_id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getPlaylist_id() {
    return playlist_id;
  }

  public void setPlaylist_id(Long playlist_id) {
    this.playlist_id = playlist_id;
  }

  public String getLibrary_ref_id() {
    return library_ref_id;
  }

  public void setLibrary_ref_id(String library_ref_id) {
    this.library_ref_id = library_ref_id;
  }
}
