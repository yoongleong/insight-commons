package co.insight.common.model.comment;

import co.insight.common.model.user.User;
import java.io.Serializable;

/*
representing a snapshot of a course's question thread,
i.e. a combination of teacher/publisher information, question comment thread information
and comment information
 */
public class Classroom implements Serializable {

  private static final long serialVersionUID = -6782850695241595658L;
  private String threadId;
  private String name;
  private User publisher;
  private CommentEntry comment;
  private Long commentCount;

  public Classroom() {
  }

  public Classroom(User user, CommentThread thread) {
    this.publisher = user;
    this.name = thread.getName();
    this.threadId = thread.getId();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Classroom classroom = (Classroom) o;

    if (threadId != null ? !threadId.equals(classroom.threadId) : classroom.threadId != null) {
      return false;
    }
    if (name != null ? !name.equals(classroom.name) : classroom.name != null) {
      return false;
    }
    if (publisher != null ? !publisher.equals(classroom.publisher) : classroom.publisher != null) {
      return false;
    }
    if (commentCount != null ? !commentCount.equals(classroom.commentCount) : classroom.commentCount != null) {
      return false;
    }
    return comment != null ? comment.equals(classroom.comment) : classroom.comment == null;
  }

  @Override
  public int hashCode() {
    int result = threadId != null ? threadId.hashCode() : 0;
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (publisher != null ? publisher.hashCode() : 0);
    result = 31 * result + (comment != null ? comment.hashCode() : 0);
    result = 31 * result + (commentCount != null ? commentCount.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Classroom{" +
        "threadId='" + threadId + '\'' +
        "name='" + name + '\'' +
        ", publisher=" + publisher +
        ", comment=" + comment +
        ", commentCount=" + commentCount +
        '}';
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public User getPublisher() {
    return publisher;
  }

  public void setPublisher(User publisher) {
    this.publisher = publisher;
  }

  public CommentEntry getComment() {
    return comment;
  }

  public void setComment(CommentEntry comment) {
    this.comment = comment;
  }

  public String getThreadId() {
    return threadId;
  }

  public void setThreadId(String threadId) {
    this.threadId = threadId;
  }

  public Long getCommentCount() {
    return commentCount;
  }

  public void setCommentCount(Long commentCount) {
    this.commentCount = commentCount;
  }
}
