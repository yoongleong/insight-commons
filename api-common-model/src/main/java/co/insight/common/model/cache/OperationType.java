package co.insight.common.model.cache;

import java.io.Serializable;

public enum OperationType implements Serializable{
  REFRESH,
  DELETE;
}
