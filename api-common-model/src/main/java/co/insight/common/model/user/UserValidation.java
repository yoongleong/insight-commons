package co.insight.common.model.user;

import java.io.Serializable;

public class UserValidation implements Serializable {

  private String email;
  private String password;

  public UserValidation() {
  }

  public UserValidation(String email, String password) {
    this.email = email;
    this.password = password;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

}
