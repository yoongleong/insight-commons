package co.insight.common.model.course;


import java.io.Serializable;
import java.util.List;

public class FeaturedCourses implements Serializable {
  private static final long serialVersionUID = 1L;

  private String description;

  private List<Course> courses;

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public List<Course> getCourses() {
    return courses;
  }

  public void setCourses(List<Course> courses) {
    this.courses = courses;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    FeaturedCourses that = (FeaturedCourses) o;

    if (!description.equals(that.description)) {
      return false;
    }
    return courses.equals(that.courses);
  }

  @Override
  public int hashCode() {
    int result = description.hashCode();
    result = 31 * result + courses.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "FeaturedCourses{" +
        "description='" + description + '\'' +
        ", courses=" + courses +
        '}';
  }
}
