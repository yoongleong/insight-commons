package co.insight.common.model.userdevice;

/**
 * OS type
 * a : Apple - iOS
 * g : Google - Android
 */
public enum OsType {
  a,
  g
}
