package co.insight.common.model;

import java.io.Serializable;
import java.util.List;

public class TimestampIds implements Serializable {
  private static final long serialVersionUID = 1;

  // UTC
  private Long timestamp;
  private List<String> ids;

  public Long getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Long timestamp) {
    this.timestamp = timestamp;
  }

  public List<String> getIds() {
    return ids;
  }

  public void setIds(List<String> ids) {
    this.ids = ids;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    TimestampIds that = (TimestampIds) o;

    if (timestamp != null ? !timestamp.equals(that.timestamp) : that.timestamp != null) {
      return false;
    }
    return ids != null ? ids.equals(that.ids) : that.ids == null;
  }

  @Override
  public String toString() {
    return "TimestampIds{" +
        "timestamp=" + timestamp +
        ", ids=" + ids +
        '}';
  }

  @Override
  public int hashCode() {
    int result = timestamp != null ? timestamp.hashCode() : 0;
    result = 31 * result + (ids != null ? ids.hashCode() : 0);
    return result;
  }

}
