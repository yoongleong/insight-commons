package co.insight.common.model.user;

import co.insight.common.model.ObjectUtils;
import co.insight.common.model.common.Picture;
import co.insight.common.model.common.ShareInfo;
import co.insight.common.model.region.City;
import java.io.Serializable;

public class User implements Serializable {
  public static final long LATEST_CACHE_VERSION = 20l;
  private static final long serialVersionUID = 2474116498183170107L;
  private String id; //"ref_id" as in the users table of 2 Nov 2017
  private String pref_id;
  private String name;
  private String description; //This is "tagline" as seen in Android UI as of 19 Oct 2017.
  private City region;
  private Picture picture;
  private Picture picture_medium;
  private Picture publisher_picture_small;
  private Picture publisher_picture_medium;
  private boolean is_publisher;
  private String publisher_description;
  private Long play_count;
  private Long follower_count;
  private Boolean followed_by_me;
  private Boolean followed_allowing_notifications;
  private Boolean allow_messaging;
  private String public_url;
  private String publisher_website;
  private ShareInfo share_info;
  private Boolean new_terms_available;
  private Long created_at;

  private long cache_version;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    User that = (User) o;
    return ObjectUtils.equals(getPref_id(), that.getPref_id());
  }

  @Override
  public int hashCode() {
    return ObjectUtils.hash(getPref_id(), 31);
  }

  @Override
  public String toString() {
    return "User{" +
        "id='" + id + '\'' +
        ", pref_id='" + pref_id + '\'' +
        ", name='" + name + '\'' +
        ", description='" + description + '\'' +
        ", region=" + region +
        ", picture=" + picture +
        ", picture_medium=" + picture_medium +
        ", publisher_picture_small=" + publisher_picture_small +
        ", publisher_picture_medium=" + publisher_picture_medium +
        ", is_publisher=" + is_publisher +
        ", publisher_description='" + publisher_description + '\'' +
        ", play_count=" + play_count +
        ", follower_count=" + follower_count +
        ", followed_by_me=" + followed_by_me +
        ", followed_allowing_notifications=" + followed_allowing_notifications +
        ", allow_messaging=" + allow_messaging +
        ", public_url='" + public_url + '\'' +
        ", publisher_website='" + publisher_website + '\'' +
        ", share_info=" + share_info +
        ", new_terms_available=" + new_terms_available +
        ", cache_version=" + cache_version +
        '}';
  }

  public String getId() {
    return id;
  }

  public String getRefId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public void setRefId(String refId) {
    this.id = refId;
  }

  public String getPref_id() {
    return pref_id;
  }

  public void setPref_id(String pref_id) {
    this.pref_id = pref_id;
  }

  public String getPrefId() {
    return pref_id;
  }

  public void setPrefId(String prefId) {
    this.pref_id = prefId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public City getRegion() {
    return region;
  }

  public void setRegion(City region) {
    this.region = region;
  }

  public Picture getPicture() {
    return picture;
  }

  public void setPicture(Picture picture) {
    this.picture = picture;
  }

  public Picture getPicture_medium() {
    return picture_medium;
  }

  public void setPicture_medium(Picture picture_medium) {
    this.picture_medium = picture_medium;
  }

  public Picture getPictureMedium() {
    return picture_medium;
  }

  public void setPictureMedium(Picture pictureMedium) {
    this.picture_medium = pictureMedium;
  }

  public Picture getPublisher_picture_small() {
    return publisher_picture_small;
  }

  public void setPublisher_picture_small(Picture publisher_picture_small) {
    this.publisher_picture_small = publisher_picture_small;
  }

  public Picture getPublisherPictureSmall() {
    return publisher_picture_small;
  }

  public void setPublisherPictureSmall(Picture publisherPictureSmall) {
    this.publisher_picture_small = publisherPictureSmall;
  }

  public Picture getPublisher_picture_medium() {
    return publisher_picture_medium;
  }

  public void setPublisher_picture_medium(Picture publisher_picture_medium) {
    this.publisher_picture_medium = publisher_picture_medium;
  }

  public Picture getPublisherPictureMedium() {
    return publisher_picture_medium;
  }

  public void setPublisherPictureMedium(Picture publisherPictureMedium) {
    this.publisher_picture_medium = publisherPictureMedium;
  }

  public boolean isIs_publisher() {
    return is_publisher;
  }

  public void setIs_publisher(boolean is_publisher) {
    this.is_publisher = is_publisher;
  }

  public boolean isPublisher() {
    return is_publisher;
  }

  public void setIsPublisher(boolean isPublisher) {
    this.is_publisher = isPublisher;
  }

  public String getPublisher_description() {
    return publisher_description;
  }

  public void setPublisher_description(String publisher_description) {
    this.publisher_description = publisher_description;
  }

  public String getPublisherDescription() {
    return publisher_description;
  }

  public void setPublisherDescription(String publisherDescription) {
    this.publisher_description = publisherDescription;
  }

  public Long getPlay_count() {
    return play_count;
  }

  public void setPlay_count(Long play_count) {
    this.play_count = play_count;
  }

  public Long getPlayCount() {
    return play_count;
  }

  public void setPlayCount(Long playCount) {
    this.play_count = playCount;
  }

  public Long getFollower_count() {
    return follower_count;
  }

  public void setFollower_count(Long follower_count) {
    this.follower_count = follower_count;
  }

  public Long getFollowerCount() {
    return follower_count;
  }

  public void setFollowerCount(Long followerCount) {
    this.follower_count = followerCount;
  }

  public Boolean isFollowed_by_me() {
    return followed_by_me;
  }

  public void setFollowed_by_me(Boolean followed_by_me) {
    this.followed_by_me = followed_by_me;
  }

  public Boolean isFollowedByMe() {
    return followed_by_me;
  }

  public void setFollowedByMe(Boolean followedByMe) {
    this.followed_by_me = followedByMe;
  }

  public String getPublic_url() {
    return public_url;
  }

  public void setPublic_url(String public_url) {
    this.public_url = public_url;
  }

  public String getPublicUrl() {
    return public_url;
  }

  public void setPublicUrl(String publicUrl) {
    this.public_url = publicUrl;
  }

  public String getPublisher_website() {
    return publisher_website;
  }

  public void setPublisher_website(String publisher_website) {
    this.publisher_website = publisher_website;
  }

  public String getPublisherWebsite() {
    return publisher_website;
  }

  public void setPublisherWebsite(String publisherWebsite) {
    this.publisher_website = publisherWebsite;
  }

  public Boolean getFollowed_allowing_notifications() {
    return followed_allowing_notifications;
  }

  public void setFollowed_allowing_notifications(Boolean followed_allowing_notifications) {
    this.followed_allowing_notifications = followed_allowing_notifications;
  }

  public Boolean getFollowedAllowingNotifications() {
    return followed_allowing_notifications;
  }

  public void setFollowedAllowingNotifications(Boolean followedAllowingNotifications) {
    this.followed_allowing_notifications = followedAllowingNotifications;
  }

  public Boolean getAllow_messaging() {
    return allow_messaging;
  }

  public void setAllow_messaging(Boolean allow_messaging) {
    this.allow_messaging = allow_messaging;
  }

  public Boolean getAllowMessaging() {
    return allow_messaging;
  }

  public void setAllowMessaging(Boolean allowMessaging) {
    this.allow_messaging = allowMessaging;
  }

  public ShareInfo getShare_info() {
    return share_info;
  }

  public void setShare_info(ShareInfo share_info) {
    this.share_info = share_info;
  }

  public ShareInfo getShareInfo() {
    return share_info;
  }

  public void setShareInfo(ShareInfo shareInfo) {
    this.share_info = shareInfo;
  }

  public Boolean isNew_terms_available() {
    return new_terms_available;
  }

  public void setNew_terms_available(Boolean new_terms_available) {
    this.new_terms_available = new_terms_available;
  }

  public Boolean isNewTermsAvailable() {
    return new_terms_available;
  }

  public void setNewTermsAvailable(Boolean newTermsAvailable) {
    this.new_terms_available = newTermsAvailable;
  }

  public long getCache_version() {
    return cache_version;
  }

  public void setCache_version(long cache_version) {
    this.cache_version = cache_version;
  }

  public long getCacheVersion() {
    return cache_version;
  }

  public void setCacheVersion(long cacheVersion) {
    this.cache_version = cacheVersion;
  }

  public Long getCreated_at() {
    return created_at;
  }

  public void setCreated_at(Long created_at) {
    this.created_at = created_at;
  }
}
