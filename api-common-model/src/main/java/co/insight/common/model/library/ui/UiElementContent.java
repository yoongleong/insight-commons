package co.insight.common.model.library.ui;

import co.insight.common.model.common.Language;
import co.insight.common.model.common.ShareInfo;
import co.insight.common.model.course.Course;
import co.insight.common.model.library.LibraryItemSummary;
import co.insight.common.model.user.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UiElementContent implements Serializable {
  private static final long serialVersionUID = 1L;

  private String title;

  private String picture_url;

  private String icon_url;

  private String description;

  private String feature_id;

  private String follow_id;

  private Long follow_count;

  private Long interest_count;

  private Long supporter_count;

  private String category_group_title;

  private String category_group_uri;

  private UiElementContentType content_type;

  private String see_all_uri;

  private String see_all_title;

  private List<UiElementItem> items;

  private List<LibraryItemSummary> library_items;

  private List<Course> course_items;

  private List<User> publishers;

  private List<Language> languages;

  private Boolean followed_by_me;

  private Boolean followed_privately;

  private String publicUrl;

  private ShareInfo share_info;

  private Boolean is_beta;

  private Boolean is_new;

  private Boolean is_playlist;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getPicture_url() {
    return picture_url;
  }

  public void setPicture_url(String picture_url) {
    this.picture_url = picture_url;
  }

  public String getIcon_url() {
    return icon_url;
  }

  public void setIcon_url(String icon_url) {
    this.icon_url = icon_url;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getFeature_id() {
    return feature_id;
  }

  public void setFeature_id(String feature_id) {
    this.feature_id = feature_id;
  }

  public String getFollow_id() {
    return follow_id;
  }

  public void setFollow_id(String follow_id) {
    this.follow_id = follow_id;
  }

  public Long getFollow_count() {
    return follow_count;
  }

  public void setFollow_count(Long follow_count) {
    this.follow_count = follow_count;
  }

  public Long getSupporter_count() {
    return supporter_count;
  }

  public void setSupporter_count(Long supporter_count) {
    this.supporter_count = supporter_count;
  }

  public Long getInterest_count() {
    return interest_count;
  }

  public void setInterest_count(Long interest_count) {
    this.interest_count = interest_count;
  }

  public String getCategory_group_title() {
    return category_group_title;
  }

  public void setCategory_group_title(String category_group_title) {
    this.category_group_title = category_group_title;
  }

  public String getCategory_group_uri() {
    return category_group_uri;
  }

  public void setCategory_group_uri(String category_group_uri) {
    this.category_group_uri = category_group_uri;
  }

  public UiElementContentType getContent_type() {
    return content_type;
  }

  public void setContent_type(UiElementContentType content_type) {
    this.content_type = content_type;
  }

  public String getSee_all_uri() {
    return see_all_uri;
  }

  public void setSee_all_uri(String see_all_uri) {
    this.see_all_uri = see_all_uri;
  }

  public String getSee_all_title() {
    return see_all_title;
  }

  public void setSee_all_title(String see_all_title) {
    this.see_all_title = see_all_title;
  }

  public List<UiElementItem> getItems() {
    return items;
  }

  public void setItems(List<UiElementItem> items) {
    this.items = items;
  }

  public List<LibraryItemSummary> getLibrary_items() {
    return library_items;
  }

  public void setLibrary_items(List<LibraryItemSummary> library_items) {
    final List<LibraryItemSummary> filtered = new ArrayList<>();
    for (LibraryItemSummary item : library_items) {
      if (item.getIs_not_found() == null || item.getIs_not_found() == false) {
        filtered.add(item);
      }
    }
    this.library_items = filtered;
  }

  public List<Course> getCourse_items() {
    return course_items;
  }

  public void setCourse_items(List<Course> course_items) {
    this.course_items = course_items;
  }

  public List<User> getPublishers() {
    return publishers;
  }

  public void setPublishers(List<User> publishers) {
    this.publishers = publishers;
  }

  public List<Language> getLanguages() {
    return languages;
  }

  public void setLanguages(List<Language> languages) {
    this.languages = languages;
  }

  public Boolean getFollowed_by_me() {
    return followed_by_me;
  }

  public void setFollowed_by_me(Boolean followed_by_me) {
    this.followed_by_me = followed_by_me;
  }

  public Boolean getFollowed_privately() {
    return followed_privately;
  }

  public void setFollowed_privately(Boolean followed_privately) {
    this.followed_privately = followed_privately;
  }

  public String getPublicUrl() {
    return publicUrl;
  }

  public void setPublicUrl(String publicUrl) {
    this.publicUrl = publicUrl;
  }

  public ShareInfo getShare_info() {
    return share_info;
  }

  public void setShare_info(ShareInfo share_info) {
    this.share_info = share_info;
  }

  public Boolean getIs_beta() {
    return is_beta;
  }

  public void setIs_beta(Boolean is_beta) {
    this.is_beta = is_beta;
  }

  public Boolean getIs_playlist() {
    return is_playlist;
  }

  public void setIs_playlist(Boolean is_playlist) {
    this.is_playlist = is_playlist;
  }

  public Boolean getIs_new() {
    return is_new;
  }

  public void setIs_new(Boolean is_new) {
    this.is_new = is_new;
  }
}
