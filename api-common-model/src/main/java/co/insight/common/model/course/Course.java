package co.insight.common.model.course;


import co.insight.common.model.audio.AudioPlay;
import co.insight.common.model.common.Language;
import co.insight.common.model.library.AudioTag;
import co.insight.common.model.library.Rating;
import co.insight.common.model.user.User;
import java.io.Serializable;
import java.util.List;

public class Course implements Serializable {
  private static final long serialVersionUID = 1L;

  private String id;

  private String name;

  private String name_html;

  private String description;

  private String learn_description;

  private String publisher_id;

  private User publisher;

  private Integer days;

  private Integer minutes_per_day;

  private Language lang;

  private String picture;

  private String background_picture;

  private AudioPlay audioPlay;

  private Rating rating;

  private Long graduates;

  private AudioTag category;

  private AudioTag benefit;

  private AudioTag origin;

  private AudioTag practice;

  private String review_thread;

  private String question_thread;

  private List<CourseDay> course_days;

  private CoursePurchaseTier purchase_tier;

  private Integer total_day_track_size;

  private String share_vanity_url;

  private String brand_hex_color;

  private String title_hex_color;

  private String publisher_intro;

  private String publisher_first_name;

  private CourseApplePurchase apple_in_app_purchase;

  public Course() {
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Course course = (Course) o;

    if (id != null ? !id.equals(course.id) : course.id != null) {
      return false;
    }
    if (name != null ? !name.equals(course.name) : course.name != null) {
      return false;
    }
    if (name_html != null ? !name_html.equals(course.name_html) : course.name_html != null) {
      return false;
    }
    if (description != null ? !description.equals(course.description) : course.description != null) {
      return false;
    }
    if (learn_description != null ? !learn_description.equals(course.learn_description) : course.learn_description != null) {
      return false;
    }
    if (publisher_id != null ? !publisher_id.equals(course.publisher_id) : course.publisher_id != null) {
      return false;
    }
    if (publisher != null ? !publisher.equals(course.publisher) : course.publisher != null) {
      return false;
    }
    if (days != null ? !days.equals(course.days) : course.days != null) {
      return false;
    }
    if (minutes_per_day != null ? !minutes_per_day.equals(course.minutes_per_day) : course.minutes_per_day != null) {
      return false;
    }
    if (lang != null ? !lang.equals(course.lang) : course.lang != null) {
      return false;
    }
    if (picture != null ? !picture.equals(course.picture) : course.picture != null) {
      return false;
    }
    if (background_picture != null ? !background_picture.equals(course.background_picture) : course.background_picture != null) {
      return false;
    }
    if (audioPlay != null ? !audioPlay.equals(course.audioPlay) : course.audioPlay != null) {
      return false;
    }
    if (rating != null ? !rating.equals(course.rating) : course.rating != null) {
      return false;
    }
    if (graduates != null ? !graduates.equals(course.graduates) : course.graduates != null) {
      return false;
    }
    if (category != null ? !category.equals(course.category) : course.category != null) {
      return false;
    }
    if (benefit != null ? !benefit.equals(course.benefit) : course.benefit != null) {
      return false;
    }
    if (origin != null ? !origin.equals(course.origin) : course.origin != null) {
      return false;
    }
    if (practice != null ? !practice.equals(course.practice) : course.practice != null) {
      return false;
    }
    if (review_thread != null ? !review_thread.equals(course.review_thread) : course.review_thread != null) {
      return false;
    }
    if (question_thread != null ? !question_thread.equals(course.question_thread) : course.question_thread != null) {
      return false;
    }
    if (course_days != null ? !course_days.equals(course.course_days) : course.course_days != null) {
      return false;
    }
    if (purchase_tier != course.purchase_tier) {
      return false;
    }
    if (total_day_track_size != null ? !total_day_track_size.equals(course.total_day_track_size) : course.total_day_track_size != null) {
      return false;
    }
    if (share_vanity_url != null ? !share_vanity_url.equals(course.share_vanity_url) : course.share_vanity_url != null) {
      return false;
    }
    if (brand_hex_color != null ? !brand_hex_color.equals(course.brand_hex_color) : course.brand_hex_color != null) {
      return false;
    }
    if (title_hex_color != null ? !title_hex_color.equals(course.title_hex_color) : course.title_hex_color != null) {
      return false;
    }
    if (publisher_intro != null ? !publisher_intro.equals(course.publisher_intro) : course.publisher_intro != null) {
      return false;
    }
    return publisher_first_name != null ? publisher_first_name.equals(course.publisher_first_name) : course.publisher_first_name == null;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (name_html != null ? name_html.hashCode() : 0);
    result = 31 * result + (description != null ? description.hashCode() : 0);
    result = 31 * result + (learn_description != null ? learn_description.hashCode() : 0);
    result = 31 * result + (publisher_id != null ? publisher_id.hashCode() : 0);
    result = 31 * result + (publisher != null ? publisher.hashCode() : 0);
    result = 31 * result + (days != null ? days.hashCode() : 0);
    result = 31 * result + (minutes_per_day != null ? minutes_per_day.hashCode() : 0);
    result = 31 * result + (lang != null ? lang.hashCode() : 0);
    result = 31 * result + (picture != null ? picture.hashCode() : 0);
    result = 31 * result + (background_picture != null ? background_picture.hashCode() : 0);
    result = 31 * result + (audioPlay != null ? audioPlay.hashCode() : 0);
    result = 31 * result + (rating != null ? rating.hashCode() : 0);
    result = 31 * result + (graduates != null ? graduates.hashCode() : 0);
    result = 31 * result + (category != null ? category.hashCode() : 0);
    result = 31 * result + (benefit != null ? benefit.hashCode() : 0);
    result = 31 * result + (origin != null ? origin.hashCode() : 0);
    result = 31 * result + (practice != null ? practice.hashCode() : 0);
    result = 31 * result + (review_thread != null ? review_thread.hashCode() : 0);
    result = 31 * result + (question_thread != null ? question_thread.hashCode() : 0);
    result = 31 * result + (course_days != null ? course_days.hashCode() : 0);
    result = 31 * result + (purchase_tier != null ? purchase_tier.hashCode() : 0);
    result = 31 * result + (total_day_track_size != null ? total_day_track_size.hashCode() : 0);
    result = 31 * result + (share_vanity_url != null ? share_vanity_url.hashCode() : 0);
    result = 31 * result + (brand_hex_color != null ? brand_hex_color.hashCode() : 0);
    result = 31 * result + (title_hex_color != null ? title_hex_color.hashCode() : 0);
    result = 31 * result + (publisher_intro != null ? publisher_intro.hashCode() : 0);
    result = 31 * result + (publisher_first_name != null ? publisher_first_name.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Course{" +
        "id='" + id + '\'' +
        ", name='" + name + '\'' +
        ", name_html='" + name_html + '\'' +
        ", description='" + description + '\'' +
        ", learn_description='" + learn_description + '\'' +
        ", publisher_id='" + publisher_id + '\'' +
        ", publisher=" + publisher +
        ", days=" + days +
        ", minutes_per_day=" + minutes_per_day +
        ", lang=" + lang +
        ", picture='" + picture + '\'' +
        ", background_picture='" + background_picture + '\'' +
        ", audioPlay=" + audioPlay +
        ", rating=" + rating +
        ", graduates=" + graduates +
        ", category=" + category +
        ", benefit=" + benefit +
        ", origin=" + origin +
        ", practice=" + practice +
        ", review_thread='" + review_thread + '\'' +
        ", question_thread='" + question_thread + '\'' +
        ", course_days=" + course_days +
        ", purchase_tier=" + purchase_tier +
        ", total_day_track_size=" + total_day_track_size +
        ", share_vanity_url='" + share_vanity_url + '\'' +
        ", brand_hex_color='" + brand_hex_color + '\'' +
        ", title_hex_color='" + title_hex_color + '\'' +
        ", publisher_intro='" + publisher_intro + '\'' +
        ", publisher_first_name='" + publisher_first_name + '\'' +
        '}';
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getName_html() {
    return name_html;
  }

  public void setName_html(String name_html) {
    this.name_html = name_html;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getLearn_description() {
    return learn_description;
  }

  public void setLearn_description(String learn_description) {
    this.learn_description = learn_description;
  }

  public String getPublisher_id() {
    return publisher_id;
  }

  public void setPublisher_id(String publisher_id) {
    this.publisher_id = publisher_id;
  }

  public User getPublisher() {
    return publisher;
  }

  public void setPublisher(User publisher) {
    this.publisher = publisher;
  }

  public Integer getDays() {
    return days;
  }

  public void setDays(Integer days) {
    this.days = days;
  }

  public Integer getMinutes_per_day() {
    return minutes_per_day;
  }

  public void setMinutes_per_day(Integer minutes_per_day) {
    this.minutes_per_day = minutes_per_day;
  }

  public Language getLang() {
    return lang;
  }

  public void setLang(Language lang) {
    this.lang = lang;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public String getBackground_picture() {
    return background_picture;
  }

  public void setBackground_picture(String background_picture) {
    this.background_picture = background_picture;
  }

  public AudioPlay getAudioPlay() {
    return audioPlay;
  }

  public void setAudioPlay(AudioPlay audioPlay) {
    this.audioPlay = audioPlay;
  }

  public List<CourseDay> getCourse_days() {
    return course_days;
  }

  public void setCourse_days(List<CourseDay> course_days) {
    this.course_days = course_days;
  }

  public Rating getRating() {
    return rating;
  }

  public void setRating(Rating rating) {
    this.rating = rating;
  }

  public Long getGraduates() {
    return graduates;
  }

  public void setGraduates(Long graduates) {
    this.graduates = graduates;
  }

  public AudioTag getCategory() {
    return category;
  }

  public void setCategory(AudioTag category) {
    this.category = category;
  }

  public AudioTag getBenefit() {
    return benefit;
  }

  public void setBenefit(AudioTag benefit) {
    this.benefit = benefit;
  }

  public AudioTag getOrigin() {
    return origin;
  }

  public void setOrigin(AudioTag origin) {
    this.origin = origin;
  }

  public AudioTag getPractice() {
    return practice;
  }

  public void setPractice(AudioTag practice) {
    this.practice = practice;
  }

  public String getReview_thread() {
    return review_thread;
  }

  public void setReview_thread(String review_thread) {
    this.review_thread = review_thread;
  }

  public String getQuestion_thread() {
    return question_thread;
  }

  public void setQuestion_thread(String question_thread) {
    this.question_thread = question_thread;
  }

  public CoursePurchaseTier getPurchase_tier() {
    return purchase_tier;
  }

  public void setPurchase_tier(CoursePurchaseTier purchase_tier) {
    this.purchase_tier = purchase_tier;
  }

  public Integer getTotal_day_track_size() {
    return total_day_track_size;
  }

  public void setTotal_day_track_size(Integer total_day_track_size) {
    this.total_day_track_size = total_day_track_size;
  }

  public String getShare_vanity_url() {
    return share_vanity_url;
  }

  public void setShare_vanity_url(String share_vanity_url) {
    this.share_vanity_url = share_vanity_url;
  }

  public String getBrand_hex_color() {
    return brand_hex_color;
  }

  public void setBrand_hex_color(String brand_hex_color) {
    this.brand_hex_color = brand_hex_color;
  }

  public String getTitle_hex_color() {
    return title_hex_color;
  }

  public void setTitle_hex_color(String title_hex_color) {
    this.title_hex_color = title_hex_color;
  }

  public String getPublisher_intro() {
    return publisher_intro;
  }

  public void setPublisher_intro(String publisher_intro) {
    this.publisher_intro = publisher_intro;
  }

  public String getPublisher_first_name() {
    return publisher_first_name;
  }

  public void setPublisher_first_name(String publisher_first_name) {
    this.publisher_first_name = publisher_first_name;
  }

  public CourseApplePurchase getApple_in_app_purchase() {
    return apple_in_app_purchase;
  }

  public void setApple_in_app_purchase(CourseApplePurchase apple_in_app_purchase) {
    this.apple_in_app_purchase = apple_in_app_purchase;
  }
}
