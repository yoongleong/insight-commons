package co.insight.common.model.cache;

import java.io.Serializable;

public enum OperationTarget implements Serializable{

  // user api
  USER,
  GROUP,
  FRIEND,

  //purchases api
  PURCHASES,

  // library api
  LIBRARY_ITEM,
  PUBLISHER,
  CATEGORY,

  // comment api
  COMMENT_THREAD,
  COMMENT_THREAD_MEDIA,
  COMMENT_ENTRY,
  COMMENT_REPLY_COUNT,

  // course api
  COURSE,
  COURSE_DAY,

  ;
}
