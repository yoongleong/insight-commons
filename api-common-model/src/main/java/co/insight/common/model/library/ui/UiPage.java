package co.insight.common.model.library.ui;

import java.io.Serializable;
import java.util.List;

public class UiPage implements Serializable {
  private static final long serialVersionUID = 1L;

  private List<UiElement> ui_elems;

  private UiMetadata metadata;

  private long cache_version;

  public long getCache_version() {
    return cache_version;
  }

  public void setCache_version(long cache_version) {
    this.cache_version = cache_version;
  }

  public List<UiElement> getUi_elems() {
    return ui_elems;
  }

  public void setUi_elems(List<UiElement> ui_elems) {
    this.ui_elems = ui_elems;
  }

  public UiMetadata getMetadata() {
    return metadata;
  }

  public void setMetadata(UiMetadata metadata) {
    this.metadata = metadata;
  }
}
