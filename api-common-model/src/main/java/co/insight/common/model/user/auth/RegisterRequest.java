package co.insight.common.model.user.auth;

import java.io.Serializable;

public class RegisterRequest implements Serializable {

  private static final long serialVersionUID = -749038365478530239L;
  private String name;
  private String email;
  private String pwd;

  public RegisterRequest() {
  }

  public RegisterRequest(String email, String password, String name) {
    this.email = email;
    this.pwd = pwd;
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return pwd;
  }

  public void setPassword(String password) {
    this.pwd = password;
  }
}
