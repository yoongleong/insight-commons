package co.insight.common.model.co.insight.common.model.payment;

import java.util.ArrayList;
import java.util.List;

public class PublisherDonations {

  private List<PublisherDonation> donations;

  public PublisherDonations() {
    this.donations = new ArrayList<>();
  }

  public List<PublisherDonation> getDonations() {
    return donations;
  }

  public void setDonations(List<PublisherDonation> donations) {
    this.donations = donations;
  }
}
