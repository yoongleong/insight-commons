package co.insight.common.model.course;

import co.insight.common.model.audio.AudioPlay;
import co.insight.common.model.user.User;
import java.io.Serializable;
import java.util.List;

public class CourseDay implements Serializable {
  private static final long serialVersionUID = 1L;

  private String id;

  private String course_id;

  private String title;

  private AudioPlay audioPlay;

  private User publisher;

  private int day;

  private String outline;

  private String question;

  private List<CourseDayQoption> course_question_options;

  private List<CourseDayQoption> course_question_skip_option;

  private Integer track_size;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCourse_id() {
    return course_id;
  }

  public void setCourse_id(String course_id) {
    this.course_id = course_id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public AudioPlay getAudioPlay() {
    return audioPlay;
  }

  public void setAudioPlay(AudioPlay audioPlay) {
    this.audioPlay = audioPlay;
  }

  public User getPublisher() {
    return publisher;
  }

  public void setPublisher(User publisher) {
    this.publisher = publisher;
  }

  public String getOutline() {
    return outline;
  }

  public void setOutline(String outline) {
    this.outline = outline;
  }

  public String getQuestion() {
    return question;
  }

  public void setQuestion(String question) {
    this.question = question;
  }

  public int getDay() {
    return day;
  }

  public void setDay(int day) {
    this.day = day;
  }

  public List<CourseDayQoption> getCourse_question_options() {
    return course_question_options;
  }

  public void setCourse_question_options(List<CourseDayQoption> course_question_options) {
    this.course_question_options = course_question_options;
  }

  public List<CourseDayQoption> getCourse_question_skip_option() {
    return course_question_skip_option;
  }

  public void setCourse_question_skip_option(List<CourseDayQoption> course_question_skip_option) {
    this.course_question_skip_option = course_question_skip_option;
  }

  public Integer getTrack_size() {
    return track_size;
  }

  public void setTrack_size(Integer track_size) {
    this.track_size = track_size;
  }
}
