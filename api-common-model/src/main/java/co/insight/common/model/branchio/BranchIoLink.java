package co.insight.common.model.branchio;

import java.io.Serializable;

public class BranchIoLink implements Serializable {
  private static final long serialVersionUID = 1L;

  private String branch_key;
  private String branch_secret;
  private String type;
  private String sdk;
  private String campaign;
  private String feature;
  private String channel;
  private String alias;
  private BranchIoData data;

  public BranchIoLink() {
  }

  public String getBranch_key() {
    return branch_key;
  }

  public void setBranch_key(String branch_key) {
    this.branch_key = branch_key;
  }

  public String getBranch_secret() {
    return branch_secret;
  }

  public void setBranch_secret(String branch_secret) {
    this.branch_secret = branch_secret;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getSdk() {
    return sdk;
  }

  public void setSdk(String sdk) {
    this.sdk = sdk;
  }

  public String getCampaign() {
    return campaign;
  }

  public void setCampaign(String campaign) {
    this.campaign = campaign;
  }

  public String getFeature() {
    return feature;
  }

  public void setFeature(String feature) {
    this.feature = feature;
  }

  public String getChannel() {
    return channel;
  }

  public void setChannel(String channel) {
    this.channel = channel;
  }

  public String getAlias() {
    return alias;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }

  public BranchIoData getData() {
    return data;
  }

  public void setData(BranchIoData data) {
    this.data = data;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    BranchIoLink that = (BranchIoLink) o;

    if (branch_key != null ? !branch_key.equals(that.branch_key) : that.branch_key != null) {
      return false;
    }
    if (branch_secret != null ? !branch_secret.equals(that.branch_secret) : that.branch_secret != null) {
      return false;
    }
    if (type != null ? !type.equals(that.type) : that.type != null) {
      return false;
    }
    if (sdk != null ? !sdk.equals(that.sdk) : that.sdk != null) {
      return false;
    }
    if (campaign != null ? !campaign.equals(that.campaign) : that.campaign != null) {
      return false;
    }
    if (feature != null ? !feature.equals(that.feature) : that.feature != null) {
      return false;
    }
    if (channel != null ? !channel.equals(that.channel) : that.channel != null) {
      return false;
    }
    if (alias != null ? !alias.equals(that.alias) : that.alias != null) {
      return false;
    }
    return data != null ? data.equals(that.data) : that.data == null;
  }

  @Override
  public int hashCode() {
    int result = branch_key != null ? branch_key.hashCode() : 0;
    result = 31 * result + (branch_secret != null ? branch_secret.hashCode() : 0);
    result = 31 * result + (type != null ? type.hashCode() : 0);
    result = 31 * result + (sdk != null ? sdk.hashCode() : 0);
    result = 31 * result + (campaign != null ? campaign.hashCode() : 0);
    result = 31 * result + (feature != null ? feature.hashCode() : 0);
    result = 31 * result + (channel != null ? channel.hashCode() : 0);
    result = 31 * result + (alias != null ? alias.hashCode() : 0);
    result = 31 * result + (data != null ? data.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "BranchIoLink{" +
        "branch_key='" + branch_key + '\'' +
        ", branch_secret='" + branch_secret + '\'' +
        ", type='" + type + '\'' +
        ", sdk='" + sdk + '\'' +
        ", campaign='" + campaign + '\'' +
        ", feature='" + feature + '\'' +
        ", channel='" + channel + '\'' +
        ", alias='" + alias + '\'' +
        ", data='" + data + '\'' +
        '}';
  }
}
