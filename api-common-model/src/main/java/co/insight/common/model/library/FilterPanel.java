package co.insight.common.model.library;

import java.io.Serializable;

public class FilterPanel implements Serializable {
  private static final long serialVersionUID = 1L;

  private boolean length_0_5;
  private boolean length_6_10;
  private boolean length_11_15;
  private boolean length_16_20;
  private boolean length_21_30;
  private boolean length_30_PLUS;

  private boolean no_religious;
  private boolean no_spiritual;

  private boolean voice_male;
  private boolean voice_female;

  private boolean background_music_yes;
  private boolean background_music_no;

  private String languageCode;

  public boolean isLength_0_5() {
    return length_0_5;
  }

  public void setLength_0_5(boolean length_0_5) {
    this.length_0_5 = length_0_5;
  }

  public boolean isLength_6_10() {
    return length_6_10;
  }

  public void setLength_6_10(boolean length_6_10) {
    this.length_6_10 = length_6_10;
  }

  public boolean isLength_11_15() {
    return length_11_15;
  }

  public void setLength_11_15(boolean length_11_15) {
    this.length_11_15 = length_11_15;
  }

  public boolean isLength_16_20() {
    return length_16_20;
  }

  public void setLength_16_20(boolean length_16_20) {
    this.length_16_20 = length_16_20;
  }

  public boolean isLength_21_30() {
    return length_21_30;
  }

  public void setLength_21_30(boolean length_21_30) {
    this.length_21_30 = length_21_30;
  }

  public boolean isLength_30_PLUS() {
    return length_30_PLUS;
  }

  public void setLength_30_PLUS(boolean length_30_PLUS) {
    this.length_30_PLUS = length_30_PLUS;
  }

  public boolean isNo_religious() {
    return no_religious;
  }

  public void setNo_religious(boolean no_religious) {
    this.no_religious = no_religious;
  }

  public boolean isNo_spiritual() {
    return no_spiritual;
  }

  public void setNo_spiritual(boolean no_spiritual) {
    this.no_spiritual = no_spiritual;
  }

  public boolean isVoice_male() {
    return voice_male;
  }

  public void setVoice_male(boolean voice_male) {
    this.voice_male = voice_male;
  }

  public boolean isVoice_female() {
    return voice_female;
  }

  public void setVoice_female(boolean voice_female) {
    this.voice_female = voice_female;
  }

  public boolean isBackground_music_yes() {
    return background_music_yes;
  }

  public void setBackground_music_yes(boolean background_music_yes) {
    this.background_music_yes = background_music_yes;
  }

  public boolean isBackground_music_no() {
    return background_music_no;
  }

  public void setBackground_music_no(boolean background_music_no) {
    this.background_music_no = background_music_no;
  }

  public String getLanguageCode() {
    return languageCode;
  }

  public void setLanguageCode(String languageCode) {
    this.languageCode = languageCode;
  }
}
