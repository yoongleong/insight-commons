package co.insight.common.model.course;

import java.io.Serializable;

public class CourseApplePurchase implements Serializable {
  private static final long serialVersionUID = 1L;

  private String multi_use;

  private String upgrade;

  public String getMulti_use() {
    return multi_use;
  }

  public void setMulti_use(String multi_use) {
    this.multi_use = multi_use;
  }

  public String getUpgrade() {
    return upgrade;
  }

  public void setUpgrade(String upgrade) {
    this.upgrade = upgrade;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    CourseApplePurchase that = (CourseApplePurchase) o;

    if (multi_use != null ? !multi_use.equals(that.multi_use) : that.multi_use != null) {
      return false;
    }
    return upgrade != null ? upgrade.equals(that.upgrade) : that.upgrade == null;
  }

  @Override
  public int hashCode() {
    int result = multi_use != null ? multi_use.hashCode() : 0;
    result = 31 * result + (upgrade != null ? upgrade.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "CourseApplePurchase{" +
        "multi_use='" + multi_use + '\'' +
        ", upgrade='" + upgrade + '\'' +
        '}';
  }
}
