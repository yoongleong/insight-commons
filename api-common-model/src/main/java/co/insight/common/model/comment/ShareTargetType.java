package co.insight.common.model.comment;

import java.io.Serializable;

public enum ShareTargetType implements Serializable {
  USERS,
  GROUPS
}
