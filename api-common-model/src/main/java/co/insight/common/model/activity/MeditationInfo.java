package co.insight.common.model.activity;

import co.insight.common.model.region.Point;
import java.util.List;

public class MeditationInfo {

  private long timestamp;
  private long meditators_now;
  private long meditators_today;
  private List<Point> activities_locations;

  public long getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

  public long getMeditators_now() {
    return meditators_now;
  }

  public void setMeditators_now(long meditators_now) {
    this.meditators_now = meditators_now;
  }

  public long getMeditators_today() {
    return meditators_today;
  }

  public void setMeditators_today(long meditators_today) {
    this.meditators_today = meditators_today;
  }

  public List<Point> getActivities_locations() {
    return activities_locations;
  }

  public void setActivities_locations(List<Point> activities_locations) {
    this.activities_locations = activities_locations;
  }
}
