package co.insight.common.model.library;

import java.io.Serializable;

public enum LibraryType implements Serializable{

  AUDIO,
  LIVE_STREAMING;
}
