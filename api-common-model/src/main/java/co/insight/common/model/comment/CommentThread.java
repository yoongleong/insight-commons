package co.insight.common.model.comment;

import co.insight.common.model.user.User;

import java.io.Serializable;
import java.util.Date;

public class CommentThread implements Serializable {

  private static final long serialVersionUID = 8421480175608065668L;
  public static final String CHAR_NOT_ALLOWED_IN_REFID = "-";

  private String id;
  private String name;
  private String owner_id; // user ref id
  private Boolean rating_allowed;
  private Boolean audio_reply_allowed;
  private Boolean comment_likes_allowed;
  private String eligible_replier;
  private String eligible_audio_replier;
  private Date created_at;
  private Date updated_at;
  private long cache_version;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    CommentThread that = (CommentThread) o;

    if (cache_version != that.cache_version) {
      return false;
    }
    if (name != null ? !name.equals(that.name) : that.name != null) {
      return false;
    }
    if (owner_id != null ? !owner_id.equals(that.owner_id) : that.owner_id != null) {
      return false;
    }
    if (rating_allowed != null ? !rating_allowed.equals(that.rating_allowed) : that.rating_allowed != null) {
      return false;
    }
    if (audio_reply_allowed != null ? !audio_reply_allowed.equals(that.audio_reply_allowed) : that.audio_reply_allowed != null) {
      return false;
    }
    if (comment_likes_allowed != null ? !comment_likes_allowed.equals(that.comment_likes_allowed) : that.comment_likes_allowed != null) {
      return false;
    }
    if (eligible_replier != null ? !eligible_replier.equals(that.eligible_replier) : that.eligible_replier != null) {
      return false;
    }
    if (eligible_audio_replier != null ? !eligible_audio_replier.equals(that.eligible_audio_replier) : that.eligible_audio_replier != null) {
      return false;
    }
    if (created_at != null ? !created_at.equals(that.created_at) : that.created_at != null) {
      return false;
    }
    return updated_at != null ? updated_at.equals(that.updated_at) : that.updated_at == null;
  }

  @Override
  public int hashCode() {
    int result = name != null ? name.hashCode() : 0;
    result = 31 * result + (owner_id != null ? owner_id.hashCode() : 0);
    result = 31 * result + (rating_allowed != null ? rating_allowed.hashCode() : 0);
    result = 31 * result + (audio_reply_allowed != null ? audio_reply_allowed.hashCode() : 0);
    result = 31 * result + (comment_likes_allowed != null ? comment_likes_allowed.hashCode() : 0);
    result = 31 * result + (eligible_replier != null ? eligible_replier.hashCode() : 0);
    result = 31 * result + (eligible_audio_replier != null ? eligible_audio_replier.hashCode() : 0);
    result = 31 * result + (created_at != null ? created_at.hashCode() : 0);
    result = 31 * result + (updated_at != null ? updated_at.hashCode() : 0);
    result = 31 * result + (int) (cache_version ^ (cache_version >>> 32));
    return result;
  }

  @Override
  public String toString() {
    return "CommentThread{" +
        ", name='" + name + '\'' +
        ", owner_id='" + owner_id + '\'' +
        ", rating_allowed=" + rating_allowed +
        ", audio_reply_allowed=" + audio_reply_allowed +
        ", comment_likes_allowed=" + comment_likes_allowed +
        ", eligible_replier='" + eligible_replier + '\'' +
        ", eligible_audio_replier='" + eligible_audio_replier + '\'' +
        ", created_at=" + created_at +
        ", updated_at=" + updated_at +
        ", cache_version=" + cache_version +
        '}';
  }

  public boolean isLegacy() {
    if (id == null) {
      return false;
    } else {
      return !id.contains(CHAR_NOT_ALLOWED_IN_REFID);
    }
  }

  public boolean isEligibleReplier(User user) {
    // All loggedin users can reply
    if (eligible_replier.equals("*")) {
      return true;
    } else {
      // Only users who's ref_id match eligible_replier value.
      return eligible_replier.equals(user.getId());
    }
  }

  public boolean isEligibleAudioReplier(User user) {
    // All loggedin users can reply audio
    if (eligible_audio_replier.equals("*")) {
      return true;
    } else {
      // Only users who's ref_id match eligible_replier value.
      return eligible_audio_replier.equals(user.getId());
    }
  }

  public long getCache_version() {
    return cache_version;
  }

  public void setCache_version(long cache_version) {
    this.cache_version = cache_version;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getOwner_id() {
    return owner_id;
  }

  public void setOwner_id(String owner_id) {
    this.owner_id = owner_id;
  }

  public Boolean getRating_allowed() {
    return rating_allowed;
  }

  public void setRating_allowed(Boolean rating_allowed) {
    this.rating_allowed = rating_allowed;
  }

  public Boolean getAudio_reply_allowed() {
    return audio_reply_allowed;
  }

  public void setAudio_reply_allowed(Boolean audio_reply_allowed) {
    this.audio_reply_allowed = audio_reply_allowed;
  }

  public Boolean getComment_likes_allowed() {
    return comment_likes_allowed;
  }

  public void setComment_likes_allowed(Boolean comment_likes_allowed) {
    this.comment_likes_allowed = comment_likes_allowed;
  }

  public String getEligible_replier() {
    return eligible_replier;
  }

  public void setEligible_replier(String eligible_replier) {
    this.eligible_replier = eligible_replier;
  }

  public String getEligible_audio_replier() {
    return eligible_audio_replier;
  }

  public void setEligible_audio_replier(String eligible_audio_replier) {
    this.eligible_audio_replier = eligible_audio_replier;
  }

  public Date getCreated_at() {
    return created_at;
  }

  public void setCreated_at(Date created_at) {
    this.created_at = created_at;
  }

  public Date getUpdated_at() {
    return updated_at;
  }

  public void setUpdated_at(Date updated_at) {
    this.updated_at = updated_at;
  }
}
