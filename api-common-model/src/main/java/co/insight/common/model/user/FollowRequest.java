package co.insight.common.model.user;

import java.io.Serializable;

public class FollowRequest implements Serializable {
  private static final long serialVersionUID = 3224193894163179707L;
  public static final long LATEST_CACHE_VERSION = 19l;

  private String target_id;

  private Boolean allow_notifications;

  private Boolean follow_privately;

  private long cache_version;

  public long getCache_version() {
    return cache_version;
  }

  public void setCache_version(long cache_version) {
    this.cache_version = cache_version;
  }

  public String getTarget_id() {
    return target_id;
  }

  public void setTarget_id(String target_id) {
    this.target_id = target_id;
  }

  public Boolean getAllow_notifications() {
    return allow_notifications;
  }

  public void setAllow_notifications(Boolean allow_notifications) {
    this.allow_notifications = allow_notifications;
  }

  public Boolean getFollow_privately() {
    return follow_privately;
  }

  public void setFollow_privately(Boolean follow_privately) {
    this.follow_privately = follow_privately;
  }

  @Override
  public String toString() {
    return "FollowRequest{" +
        "target_id='" + target_id + '\'' +
        ", allow_notifications=" + allow_notifications +
        ", follow_privately=" + follow_privately +
        '}';
  }
}
