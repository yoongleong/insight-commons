package co.insight.common.model.user;

import java.io.Serializable;

public class Milestone implements Serializable {
  private static final long serialVersionUID = 1L;

  private Long added_at;
  private String type;
  private Integer value;
  private String description;
  private String extras;
  private Long reached_at;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Milestone milestone = (Milestone) o;

    if (added_at != null ? !added_at.equals(milestone.added_at) : milestone.added_at != null) {
      return false;
    }
    if (type != null ? !type.equals(milestone.type) : milestone.type != null) {
      return false;
    }
    if (value != null ? !value.equals(milestone.value) : milestone.value != null) {
      return false;
    }
    if (description != null ? !description.equals(milestone.description) : milestone.description != null) {
      return false;
    }
    if (extras != null ? !extras.equals(milestone.extras) : milestone.extras != null) {
      return false;
    }
    return reached_at != null ? reached_at.equals(milestone.reached_at) : milestone.reached_at == null;
  }

  @Override
  public int hashCode() {
    int result = added_at != null ? added_at.hashCode() : 0;
    result = 31 * result + (type != null ? type.hashCode() : 0);
    result = 31 * result + (value != null ? value.hashCode() : 0);
    result = 31 * result + (description != null ? description.hashCode() : 0);
    result = 31 * result + (extras != null ? extras.hashCode() : 0);
    result = 31 * result + (reached_at != null ? reached_at.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Milestone{" +
        "added_at=" + added_at +
        ", type='" + type + '\'' +
        ", value=" + value +
        ", description='" + description + '\'' +
        ", extras='" + extras + '\'' +
        ", reached_at=" + reached_at +
        '}';
  }

  public Long getAdded_at() {
    return added_at;
  }

  public void setAdded_at(Long added_at) {
    this.added_at = added_at;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Integer getValue() {
    return value;
  }

  public void setValue(Integer value) {
    this.value = value;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getExtras() {
    return extras;
  }

  public void setExtras(String extras) {
    this.extras = extras;
  }

  public Long getReached_at() {
    return reached_at;
  }

  public void setReached_at(Long reached_at) {
    this.reached_at = reached_at;
  }
}
