package co.insight.common.model.course;

import java.io.Serializable;

public class CourseDayQoption implements Serializable {
  private static final long serialVersionUID = 1L;

  private Long course_day_question_id;

  private Long course_day_qoptions_id;

  private Integer position;

  private String option;

  public Long getCourse_day_question_id() {
    return course_day_question_id;
  }

  public void setCourse_day_question_id(Long course_day_question_id) {
    this.course_day_question_id = course_day_question_id;
  }

  public Long getCourse_day_qoptions_id() {
    return course_day_qoptions_id;
  }

  public void setCourse_day_qoptions_id(Long course_day_qoptions_id) {
    this.course_day_qoptions_id = course_day_qoptions_id;
  }

  public Integer getPosition() {
    return position;
  }

  public void setPosition(Integer position) {
    this.position = position;
  }

  public String getOption() {
    return option;
  }

  public void setOption(String option) {
    this.option = option;
  }
}
