package co.insight.common.model.library.ui;

import java.io.Serializable;

public class UiElement implements Serializable {
  private static final long serialVersionUID = 1L;

  private UiElementType type;

  //Flatten this out
  private UiElementContent content;

  private Boolean followed_by_me;

  private Boolean followed_with_notifications_allowed;

  private Boolean followed_privately;

  public UiElementType getType() {
    return type;
  }

  public void setType(UiElementType type) {
    this.type = type;
  }

  public UiElementContent getContent() {
    return content;
  }

  public void setContent(UiElementContent content) {
    this.content = content;
  }

  public Boolean getFollowed_by_me() {
    return followed_by_me;
  }

  public void setFollowed_by_me(Boolean followed_by_me) {
    this.followed_by_me = followed_by_me;
  }

  public Boolean getFollowed_with_notifications_allowed() {
    return followed_with_notifications_allowed;
  }

  public void setFollowed_with_notifications_allowed(Boolean followed_with_notifications_allowed) {
    this.followed_with_notifications_allowed = followed_with_notifications_allowed;
  }

  public Boolean getFollowed_privately() {
    return followed_privately;
  }

  public void setFollowed_privately(Boolean followed_privately) {
    this.followed_privately = followed_privately;
  }
}
