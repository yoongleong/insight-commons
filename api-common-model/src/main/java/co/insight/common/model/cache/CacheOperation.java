package co.insight.common.model.cache;

import java.io.Serializable;

public class CacheOperation implements Serializable {
  private static final long serialVersionUID = 3614868222557934356L;
  private OperationType op;
  private OperationTarget target;
  private String ref;

  public OperationType getOp() {
    return op;
  }

  public void setOp(OperationType op) {
    this.op = op;
  }

  public OperationTarget getTarget() {
    return target;
  }

  public void setTarget(OperationTarget target) {
    this.target = target;
  }

  public String getRef() {
    return ref;
  }

  public void setRef(String ref) {
    this.ref = ref;
  }
}
