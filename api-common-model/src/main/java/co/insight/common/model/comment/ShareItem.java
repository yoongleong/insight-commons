package co.insight.common.model.comment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ShareItem implements Serializable {

  private static final long serialVersionUID = 5868921273258585179L;

  private String message;
  private ShareTargetType target_type;
  private ShareItemType share_type;
  private String share;
  private List<String> targets = new ArrayList<String>();
  private String share_misc;
  private String metadata;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public ShareTargetType getTarget_type() {
    return target_type;
  }

  public void setTarget_type(ShareTargetType target_type) {
    this.target_type = target_type;
  }

  public ShareItemType getShare_type() {
    return share_type;
  }

  public void setShare_type(ShareItemType share_type) {
    this.share_type = share_type;
  }

  public String getShare() {
    return share;
  }

  public void setShare(String share) {
    this.share = share;
  }

  public List<String> getTargets() {
    return targets;
  }

  public void setTargets(List<String> targets) {
    this.targets = targets;
  }

  public String getShare_misc() {
    return share_misc;
  }

  public void setShare_misc(String share_misc) {
    this.share_misc = share_misc;
  }

  public String getMetadata() {
    return metadata;
  }

  public void setMetadata(String metadata) {
    this.metadata = metadata;
  }
}
