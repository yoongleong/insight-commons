package co.insight.common.model.purchases;

import java.util.Date;
import java.util.Objects;

public class DonationRecord {

  private Integer id;
  private String ref_id;
  private PurchasePlatform platform;
  private DonationItemType type;
  private DonationItemState state;
  private String user_id;
  private String publisher_id;

  private String receipt;
  private String receipt_hash;
  private Boolean is_success;

  private String gratitude_msg_thread;
  private String gratitude_msg_id;
  private String gratitude_msg_string;
  private Boolean is_private;
  private String local_currency;

  private Date purchase_date;
  private Date validate_date;
  private Boolean test_account;

  public DonationRecord() {
  }

  public DonationRecord(Integer id,
                        String ref_id,
                        PurchasePlatform platform,
                        DonationItemType type,
                        DonationItemState state,
                        String user_id,
                        String publisher_id,
                        String receipt,
                        String receipt_hash,
                        Boolean is_success,
                        String gratitude_msg_thread,
                        String gratitude_msg_id,
                        String gratitude_msg_string,
                        Boolean is_private,
                        String local_currency,
                        Date purchase_date,
                        Date validate_date,
                        Boolean test_account) {
    this.id = id;
    this.ref_id = ref_id;
    this.platform = platform;
    this.type = type;
    this.state = state;
    this.user_id = user_id;
    this.publisher_id = publisher_id;
    this.receipt = receipt;
    this.receipt_hash = receipt_hash;
    this.is_success = is_success;
    this.gratitude_msg_thread = gratitude_msg_thread;
    this.gratitude_msg_id = gratitude_msg_id;
    this.gratitude_msg_string = gratitude_msg_string;
    this.is_private = is_private;
    this.local_currency = local_currency;
    this.purchase_date = purchase_date;
    this.validate_date = validate_date;
    this.test_account = test_account;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getRef_id() {
    return ref_id;
  }

  public void setRef_id(String ref_id) {
    this.ref_id = ref_id;
  }

  public PurchasePlatform getPlatform() {
    return platform;
  }

  public void setPlatform(PurchasePlatform platform) {
    this.platform = platform;
  }

  public DonationItemType getType() {
    return type;
  }

  public void setType(DonationItemType type) {
    this.type = type;
  }

  public DonationItemState getState() {
    return state;
  }

  public void setState(DonationItemState state) {
    this.state = state;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public String getPublisher_id() {
    return publisher_id;
  }

  public void setPublisher_id(String publisher_id) {
    this.publisher_id = publisher_id;
  }

  public String getReceipt() {
    return receipt;
  }

  public void setReceipt(String receipt) {
    this.receipt = receipt;
  }

  public String getReceipt_hash() {
    return receipt_hash;
  }

  public void setReceipt_hash(String receipt_hash) {
    this.receipt_hash = receipt_hash;
  }

  public Boolean getIs_success() { return is_success; }

  public void setIs_success(Boolean is_success) {
    this.is_success = is_success;
  }

  public String getGratitude_msg_thread() { return gratitude_msg_thread; }

  public void setGratitude_msg_thread(String gratitude_msg_thread) { this.gratitude_msg_thread = gratitude_msg_thread; }

  public String getGratitude_msg_id() { return gratitude_msg_id; }

  public void setGratitude_msg_id(String gratitude_msg_id) { this.gratitude_msg_id = gratitude_msg_id; }

  public void setGratitude_msg_string(String gratitude_msg_string) {
    this.gratitude_msg_string = gratitude_msg_string;
  }

  public String getGratitude_msg_string() {
    return gratitude_msg_string;
  }

  public void setIs_private(Boolean is_private) { this.is_private = is_private; }

  public Boolean getIs_private() { return is_private; }

  public String getLocal_currency() {
    return local_currency;
  }

  public void setLocal_currency(String local_currency) {
    this.local_currency = local_currency;
  }

  public Date getPurchase_date() {
    return purchase_date;
  }

  public void setPurchase_date(Date purchase_date) {
    this.purchase_date = purchase_date;
  }

  public Date getValidate_date() {
    return validate_date;
  }

  public void setValidate_date(Date validate_date) {
    this.validate_date = validate_date;
  }

  public Boolean getTest_account() {
    return test_account;
  }

  public void setTest_account(Boolean test_account) {
    this.test_account = test_account;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || this.getClass() != obj.getClass()) {
      return false;
    }

    DonationRecord other = (DonationRecord) obj;
    return ref_id == other.getRef_id();
  }

  @Override
  public int hashCode() {
    return Objects.hash(ref_id);
  }

}
