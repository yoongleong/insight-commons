package co.insight.common.model.user.auth;

import java.io.Serializable;

public class FirebaseUpdateRequest implements Serializable {

  private static final long serialVersionUID = -6029902831545196861L;

  public enum Type {EMAIL, PASSWORD}

  protected String uid;
  protected String new_email;
  protected String new_password;

  public FirebaseUpdateRequest() {
  }

  public FirebaseUpdateRequest(String uid, Type type, String value) {

    this.uid = uid;
    switch (type) {
      case EMAIL:
        this.new_email = value;
        break;
      case PASSWORD:
        this.new_password = value;
        break;
      default:
        throw new RuntimeException("type not supported");
    }
  }

  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  public String getNew_email() {
    return new_email;
  }

  public void setNew_email(String newEmail) {
    this.new_email = newEmail;
  }

  public String getNew_password() {
    return new_password;
  }

  public void setNew_password(String newPassword) {
    this.new_password = newPassword;
  }
}
