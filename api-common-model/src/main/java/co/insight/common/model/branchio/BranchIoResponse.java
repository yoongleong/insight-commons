package co.insight.common.model.branchio;

import java.io.Serializable;

public class BranchIoResponse implements Serializable {
  private static final long serialVersionUID = 1L;

  private int status;
  private String url;
  private BranchIoError error;

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public BranchIoError getError() {
    return error;
  }

  public void setError(BranchIoError error) {
    this.error = error;
  }

  public boolean isError() {
    return error != null;
  }

  public static class BranchIoError implements Serializable {
    private static final long serialVersionUID = 1L;

    private int code;
    private String message;

    public int getCode() {
      return code;
    }

    public void setCode(int code) {
      this.code = code;
    }

    public String getMessage() {
      return message;
    }

    public void setMessage(String message) {
      this.message = message;
    }
  }
}
