package co.insight.common.model.purchases

import co.insight.common.model.course.Course
import co.insight.common.model.course.CourseUser
import java.util.*

data class UserPurchases(

  val all: MutableList<PurchaseInfo>? = ArrayList(),
  val courses: MutableList<PurchaseInfo>? = ArrayList(),
  val donations: MutableList<PurchaseInfo>? = ArrayList(),
  val packs: MutableList<PurchaseInfo>? = ArrayList()

)

data class PurchaseInfo(

  val type: PurchaseInfoType? = null, // PACK, COURSE, DONATION
  val ref_id: String? = null,
  val summary: String? = null,
  val image_url: String? = null,
  val publisher_ref_id: String? = null,
  val publisher_name: String? = null,
  val local_currency: String? = null, // "$2.99", "RM9.99", "AUD4.99"
  val state: PurchaseInfoState? = null, // RENTAL, PURCHASED, DONATED, MONTHLY, EXPIRED RENTAL, CANCELLED MONTHLY
  val purchase_date: Date? = null

)

enum class PurchaseInfoType { PACK, COURSE, DONATION, NONE }

enum class PurchaseInfoState { SINGLE_USE, COMPLETED, MULTI_USE, DONATION, RECURRING, CANCELLED_RECURRING, GIFTED, FREE, NONE }

data class CourseInfo(

  val state : CourseInfoState? = null,
  val current_day : Int? = null,
  val finished : Boolean? = null,

  val course_summary: Course? = null,
  val course_user: CourseUser? = null,
  val purchase_record: PurchaseData? = null

)

enum class CourseInfoState { RENTAL, COMPLETED_RENTAL, EXPIRED_RENTAL, PURCHASE, UPGRADE, GIFTED, FREE, NONE }