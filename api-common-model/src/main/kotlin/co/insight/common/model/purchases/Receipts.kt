package co.insight.common.model.purchases

data class AppleReceipt(
  var original_transaction_id: String? = null,
  var receipt: String? = null,
  var local_currency: String? = null
)

data class AndroidReceipt(
  var package_name: String? = null,
  var subscription_id: String? = null,
  var product_id: String? = null,
  var sku: String? = null,
  var token: String? = null,
  var local_currency: String? = null
)

data class LocalCurrencyReceipt(
  var local_currency: String? = null
)