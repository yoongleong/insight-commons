package co.insight.common.model.donations

import co.insight.common.model.purchases.DonationItemType
import co.insight.common.model.purchases.PurchasePlatform

open class DonationData(

  var platform: PurchasePlatform? = null,
  var publisher_id: String? = null,
  var type: DonationItemType? = null,
  var price_with_local_currency: String? = null,
  var gratitude_thread_id: String? = null,
  var gratitude_message: String? = null,
  var is_private: Boolean? = null

) {

  open fun getIs_private(): Boolean? {
    return is_private
  }

  open fun setIs_private(isPrivate: Boolean) {
    this.is_private = isPrivate
  }

}