package co.insight.common.model.donations

data class DonationCompleteRequest(

  var donation: DonationData? = null,

  var donation_id: String? = null,
  var purchase_date: Long? = null,
  var receipt: String? = null,
  var success: Boolean? = null

)