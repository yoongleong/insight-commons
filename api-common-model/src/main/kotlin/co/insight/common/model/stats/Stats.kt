package co.insight.common.model.stats

import org.apache.commons.lang3.Range
import org.joda.time.DateTime
import java.util.concurrent.TimeUnit

enum class TimeRange constructor(val hoursBack: Long) {
  LAST_24_HOURS(24),
  LAST_7_DAYS(TimeUnit.DAYS.toHours(7)),
  LAST_28_DAYS(TimeUnit.DAYS.toHours(28));

  companion object {
    fun buildRangeDateTime(r: TimeRange): Range<DateTime> {
      val currentTime = DateTime.now()
      val default24HoursRange = Range.between(currentTime.minusHours(24), currentTime, null)
      val retVal: Range<DateTime>
      retVal = when (r) {
        LAST_7_DAYS -> Range.between(currentTime.minusDays(7), currentTime, null)
        LAST_28_DAYS -> Range.between(currentTime.minusDays(28), currentTime, null)
        LAST_24_HOURS -> default24HoursRange
      }
      return retVal
    }
  }
}

data class PublisherStatV2(
    var duration: Long? = null,
    var total_followers: Long? = null,
    var new_followers: Long? = null,
    var new_plays: Long? = null,
    var rating_count: Long? = null
)

data class PublisherStatsForAllTimeRanges(
    var stats: List<PublisherStatV2>? = null,
    var Is_messaging_turned_on: Boolean? = null
)
