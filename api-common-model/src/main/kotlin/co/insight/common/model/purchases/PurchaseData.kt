package co.insight.common.model.purchases

open class PurchaseData(
  var platform: PurchasePlatform? = null,
  var receipt: String? = null,
  var type: PurchaseItemType? = null,
  var packs: List<String>? = null,
  var state: PurchaseItemState? = null,
  var purchase_date: Long? = null,
  var expiry_date: Long? = null,
  var original_transaction_id: String? = null
) {

  constructor(platform: PurchasePlatform, receipt: String, type: PurchaseItemType, packs: List<String>, state: PurchaseItemState) : this() {
    this.platform = platform
    this.receipt = receipt
    this.type = type
    this.packs = packs
    this.state = state
  }

}