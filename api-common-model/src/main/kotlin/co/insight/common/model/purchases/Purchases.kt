package co.insight.common.model.purchases

enum class PurchasePlatform {
  INSIGHT,
  ANDROID,
  APPLE
}

enum class DonationItemType {
  DONATION_SMALL,
  DONATION_MEDIUM,
  DONATION_LARGE,
}

enum class DonationItemState {
  PREPARE,          // preparation state - first creation
  INCOMPLETE,       // incomplete
  COMPLETE,         // complete
  ACTIVE,           // complete & success
  INVALID,          // invalid receipt
  FAILED            // failed receipt validation
}


enum class PurchaseItemType {
  // Courses
  COURSE_FREE,

  COURSE_RENTAL_10_DAY,
  COURSE_PURCHASE_10_DAY,
  COURSE_BALANCE_10_DAY,

  COURSE_RENTAL_30_DAY,
  COURSE_PURCHASE_30_DAY,
  COURSE_BALANCE_30_DAY,

  DONATION_SMALL,
  DONATION_MEDIUM,
  DONATION_LARGE,

  // Melchior
  MELCHIOR_1,
  MELCHIOR_2,
  MELCHIOR_3,
}

enum class PurchaseItemState {
  ACTIVE,   //
  INACTIVE, //
  INVALID,  // receipt is invalid, but store anyway.
  FAILED
}
