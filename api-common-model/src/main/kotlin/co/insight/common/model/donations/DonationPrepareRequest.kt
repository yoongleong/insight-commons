package co.insight.common.model.donations

data class DonationPrepareRequest(

  var donation: DonationData? = null

)