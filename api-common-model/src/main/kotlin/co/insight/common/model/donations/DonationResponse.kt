package co.insight.common.model.donations

open class DonationResponse(

  var donation_id: String? = null,
  var error: String? = null

)