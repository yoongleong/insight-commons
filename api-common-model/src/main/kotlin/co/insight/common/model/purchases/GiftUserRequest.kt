package co.insight.common.model.purchases

data class GiftUserRequest(
  var type: PurchaseItemType? = null,
  var packs: List<String>? = null
)
