package co.insight.common.model.purchases

data class PurchasesSyncRequest(

  //var user: String? = null,
  var platform: PurchasePlatform? = null,
  var current: List<PurchaseData>? = emptyList(),
  var history: List<PurchaseData>? = emptyList()
)
