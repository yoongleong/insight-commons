package co.insight.common.model.purchases

data class PurchaseRecordUpdateRequest(
  var receipt: String? = null,
  var item: String? = null,
  var new_state: PurchaseItemState? = null
)