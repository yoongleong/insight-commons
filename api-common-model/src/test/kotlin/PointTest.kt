package co.insight.common.model.region

import com.fasterxml.jackson.databind.ObjectMapper
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.Test

class PointTest {
  private val nullVal: Double? = null
  private val zero: Double = 0.0
  private val smallNum: Double = 89.98
  private val negativeNum: Double = -123456789.987654321
  private val postiveNum: Double = 123456789.987654321
  private val tenDivBy6: Double = 1.6666666667

  @Test
  fun `trimDecimals - null values are not trimmed and they remain null`() {
    val point = Point(nullVal, zero)
    assert(point.lat == null)
    assert(point.lon != null)
  }

  @Test
  fun `trimDecimals - if decimals places aren't more than 6, they are unchanged`() {
    val point = Point(smallNum, zero)
    assertThat(point.lat, equalTo(smallNum))
    assertThat(point.lon, equalTo(zero))
  }

  @Test
  fun `trimDecimals - if decimal places more than 6, they are trimmed for numbers both +ve or -ve`() {
    val point = Point(negativeNum, postiveNum)
    assertThat(point.lat, equalTo(-123456789.987654))
    assertThat(point.lon, equalTo(123456789.987654))
  }

  @Test
  fun `trimDecimals - trimming doesn't round`() {
    val point = Point(tenDivBy6, tenDivBy6)
    assertThat(point.lat, equalTo(1.666666))
    assertThat(point.lon, equalTo(1.666666))
  }

  @Test
  fun `trimDecimals - json deserialization also trims it`() {
    val json = """ {"lat": $negativeNum, "lon": $postiveNum} """
    val point = ObjectMapper().readerFor(Point::class.java).readValue<Point>(json)
    assertThat(point.lat, equalTo(-123456789.987654))
    assertThat(point.lon, equalTo(123456789.987654))
  }

  @Test
  fun `trimDecimals - json deserialization when missing field doesn't throw exception`() {
    val json = """ {"lat": $negativeNum} """
    val point = ObjectMapper().readerFor(Point::class.java).readValue<Point>(json)
    assertThat(point.lat, equalTo(-123456789.987654))
    assert(point.lon == null)
  }
}
